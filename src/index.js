import React from 'react';
import ReactDOM from 'react-dom';
import localforage from 'localforage';

// entry component

import * as serviceWorker from './serviceWorker';



// styles entry points
import './styles/index.scss';
import App from './App';

// localDataConfig
localforage.config({
  name: 'hello',
  version: 1.0,
  storeName: 'bonjour', // Should be alphanumeric, with underscores.
});

const rootEl = document.getElementById('root');

ReactDOM.render(<App />, rootEl);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


console.warn = () => {}
console.error = () => {}