import React, { useContext } from "react";
import Button from "@material-ui/core/Button/Button";
import userContext from "../contexts/userContext";
import Navbar from "../layout/Header";
import Footer from "../layout/Footer";
import { Card, Form, Carousel, Dropdown } from "react-bootstrap";
import Slider from "react-slick";
import Background from "./adminAssets/wall2.jpg"
import "./scss/adminLogin.scss";

const AdminLoginContainer = () => {
  return (
    <div className="twilight" style={{ backgroundImage: "url(" + Background + ")" }}>
      <div className="login-container">
        <div className="form-container">

          <Form>
            <Card className="card-cont" style={{ width: "35rem" }}>
              <Card.Title className="card-tit">
                <h3>Connexion</h3>
              </Card.Title>

              <Card.Body>
                <Form.Group className="adminform" controlId="formBasicEmail">
                  <Form.Label className="label">Email address</Form.Label>
                  <Form.Control
                    className=""
                    name="email"
                    placeholder="Enter email"
                  />
                </Form.Group>
                <Form.Group className="adminform" controlId="formBasicPassword">
                  <Form.Label className="label">Password</Form.Label>
                  <Form.Control
                    className=""
                    name="password"
                    type="password"
                    placeholder="Password"
                  />
                </Form.Group>
                <Button type="submit" className="se-connecter">
                  SE CONNECTER
                </Button>{" "}
              </Card.Body>
            </Card>
          </Form>{" "}
        </div>
      </div>
    </div>
  );
};

export default AdminLoginContainer;
