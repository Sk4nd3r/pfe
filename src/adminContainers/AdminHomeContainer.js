import React, { useContext, useState } from "react";
import Button from "@material-ui/core/Button/Button";
import userContext from "../contexts/userContext";
import { addProductRequest } from "../requests/product";
import useApiState from "../hooks/useApiState";
import { decodeUri } from "../utils/url";
import { Card, Col, Row, Form, Carousel, Dropdown } from "react-bootstrap";
import Slider from "react-slick";
import Background from "./adminAssets/wall2.jpg";
import { useForm } from "../hooks/useInputs";
import { isStringEmpty, isNumberEmpty } from "../utils/validation";
import { upload } from "../requests/product";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";

import "./scss/adminHome.scss";

const AdminHomeContainer = ({ location }) => {
  const [formState, formActions] = useForm({
    initialValues: {
      label: "",
      price: "",
      category: "",
      quantity: "",
      credit: "",
      marque: "",
      description: "",
      Promotion: "",
    },
    validation: {
      label: isStringEmpty,
      price: isNumberEmpty,
      category: isStringEmpty,
      quantity: isNumberEmpty,
      credit: isNumberEmpty,
      price: isNumberEmpty,
      description: isStringEmpty,
    },
  });
  const [addProductState, addProduct] = useApiState(addProductRequest);
  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const [images, setImages] = useState([]);
  const [categoryState, categoryCall] = useApiState(getCategories);
  useDidMount(() => {
    categoryCall();
    console.log(categoryState);
  });
  async function onSubmit(e) {
    e.preventDefault();
    const url = await Promise.all(images.map((image) => upload(image)));

    const {
      label,
      price,
      category,
      quantity,
      credit,
      marque,
      description,
      Promotion,
    } = formState.values;

    if (formActions.validateForm()) {
      addProduct({
        label,
        price,
        category,
        quantity,
        credit,
        marque,
        description,
        images: url.map((u) => u.url),
      });
    } else {
      formActions.setAllTouched(true);
    }
    console.log(errors);
    console.log(formState);
  }
  return (
    <div className="homeAdmin ">
      <div className="formContainer">
        <h2>Ajout Produit </h2>
        <Form onSubmit={onSubmit} className="form">
          <Form.Group className="formNom">
            <Row>
              <Col>
                {" "}
                <Form.Label>Label </Form.Label>
                <Form.Control
                  type="text"
                  name="label"
                  placeholder="Label"
                  onChange={handleChange}
                  value={values.label}
                />
              </Col>

              <Col>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  {" "}
                  <Form.Label>Catégorie </Form.Label>
                  <>
                    {" "}
                    <Form.Control
                      as="select"
                      onChange={handleChange}
                      value={values.category}
                      name="category"
                    >
                      {categoryState.data &&
                        categoryState.data.map((row) => (
                          <option key={row._id}>{row.label} </option>
                        ))}{" "}
                    </Form.Control>
                  </>
                </Form.Group>

                {/*  <Form.Control
                  type="text"
                  name="category"
                  placeholder="category"
                  onChange={handleChange}
                  value={values.category}
                /> */}
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label>Price </Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  placeholder="Price"
                  onChange={handleChange}
                  value={values.price}
                />
              </Col>
              <Col>
                <Form.Label>Quantité </Form.Label>
                <Form.Control
                  type="number"
                  name="quantity"
                  placeholder="Quantité"
                  onChange={handleChange}
                  value={values.quantity}
                />
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label>Marque </Form.Label>
                <Form.Control
                  type="text"
                  name="marque"
                  placeholder="Marque"
                  onChange={handleChange}
                  value={values.marque}
                />
              </Col>
              <Col>
                <Form.Label>Crédit </Form.Label>
                <Form.Control
                  type="number"
                  name="credit"
                  placeholder="credit"
                  onChange={handleChange}
                  value={values.credit}
                />
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label>Description </Form.Label>
                <Form.Control
                  type="text"
                  name="description"
                  placeholder="Description"
                  onChange={handleChange}
                  value={values.description}
                />
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label>Promotion </Form.Label>
                <Form.Control
                  type="text"
                  name="Promotion"
                  placeholder="Promotion"
                  onChange={handleChange}
                  value={values.Promotion}
                />
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Group>
                  <Form.File
                    id="exampleFormControlFile1"
                    label="Example file input"
                    onChange={(event) =>
                      setImages([...images, event.target.files[0]])
                    }
                  />
                </Form.Group>
              </Col>
            </Row>
          </Form.Group>
          <Button variant="primary" className="addproductbtn" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default AdminHomeContainer;
