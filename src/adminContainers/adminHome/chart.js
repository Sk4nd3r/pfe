import React, { useState, useEffect } from "react";
import { Line, Doughnut, Bar } from "react-chartjs-2";
import moment from "moment";
import * as colors from "@material-ui/core/colors";
import "../scss/chart.scss";
import useApiState from "../../hooks/useApiState";
import { getPopularCategories } from "../../requests/categories";
import { useDidMount } from "../../hooks/useLifeCycle";
import { getDailySales } from "../../requests/order";

const colorsKeys = Object.keys(colors).sort();

const Chart = () => {
  const [chartData, setChartData] = useState({});
  const [secondChartData, setSecondChartData] = useState({});
  const [popularCategoriesState, popularCategorieCall] = useApiState(
    getPopularCategories
  );

  const [dailySalesState, dailySalesCall] = useApiState(getDailySales);

  useEffect(() => {
    mySecondchart();
  }, [dailySalesState.data]);

  useEffect(() => {
    mychart();
  }, [popularCategoriesState.data]);

  useDidMount(() => {
    popularCategorieCall();
    dailySalesCall();
  });

  const mychart = () => {
    setChartData({
      labels: popularCategoriesState.data?.map((c) => c.label) || [],

      datasets: [
        {
          label: "Popularité",
          data: popularCategoriesState.data?.map((c) => c.popularity) || [],

          backgroundColor:
            popularCategoriesState.data?.map((c, i) => {
              return colors[colorsKeys[i % colorsKeys.length]][
                500 + Math.floor(i / colorsKeys.length) * 100
              ];
            }) || [],
        },
      ],
    });
  };
  const mySecondchart = () => {
    setSecondChartData({
      labels: dailySalesState.data?.map((d) => moment(d.date).format("dddd")),
      datasets: [
        {
          label: "Ventes",
          data: dailySalesState.data?.map((d) => d.sales),
          borderColor: "#F85F73",
          backgroundColor: ["transparent"],
        },
      ],
    });
  };

  return (
    <div className="container-chart">
      <div className="chart-one">
        <h1> ventes quotidiennes</h1>
        <Line width={400} height={280} data={secondChartData} />
      </div>

      <div className="chart-two">
        <h1>Catégories les plus populaires</h1>
        <Doughnut width={500} height={280} data={chartData} />
        
      </div>
    </div>
  );
};

export default Chart;
