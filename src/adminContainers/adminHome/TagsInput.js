import React, { useState, useEffect } from "react";

import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import TextField from "@material-ui/core/TextField/TextField";
import "../scss/categoriesAdmin.scss";
function TagsInput({ label, error, className, value, onChange, ...rest }) {
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue("");
  }, [value]);

  return (
    <Autocomplete
      popupIcon={<div />}
      {...rest}
      getOptionLabel={(option) => option}
      value={value}
      onChange={(e, v) => onChange(v)}
      open={false}
      options={[]}
      multiple
      disableClearable
      renderInput={(props) => (
        <TextField
          className="input-multiple"
          label={label}
          onKeyDown={(e) => {
            if (onChange) {
              if (e.key === "Enter" && inputValue) {
                e.preventDefault();
                onChange([...value, inputValue]);
              }
              if (e.key === "Backspace" && value.length && !inputValue) {
                e.preventDefault();
                onChange(
                  value.filter((r, index) => index !== value.length - 1)
                );
              }
            }
          }}
          onChange={(e) => setInputValue(e.target.value)}
          variant="outlined"
          error={!!error}
          {...props}
        />
      )}
    />
  );
}

export default TagsInput;
