import React, { useState, useEffect, useContext } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import DevicesIcon from "@material-ui/icons/Devices";
import GroupIcon from "@material-ui/icons/Group";
import { Link, withRouter, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import HomeFragments from "../homefragments/homeFragments";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import Imageside from "../adminAssets/sidebar-4.jpg";
import Modal from "@material-ui/core/Modal";
import { useForm } from "../../hooks/useInputs";
import { isStringEmpty, isNumberEmpty } from "../../utils/validation";
import { upload } from "../../requests/categories";
import {
  addCategoryRequest,
  getSubCategoryRequest,
} from "../../requests/categories";
import { toast, zoomn, bounce } from "react-toastify";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";

import { getCategories } from "../../requests/categories";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import { useDidMount } from "../../hooks/useLifeCycle";
import useApiState from "../../hooks/useApiState";
import { decodeUri } from "../../utils/url";
import { Card, Col, Row, Form, Carousel, Dropdown } from "react-bootstrap";
import Button from "@material-ui/core/Button/Button";
import FlagIcon from "@material-ui/icons/Flag";
import ImageUpload from "../imageUpload";
import {
  addProductRequest,
  getProductRequest,
  deleteProduit,
  updateProduct,
} from "../../requests/product";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import ErrorIcon from "@material-ui/icons/Error";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Tags from "./TagsInput";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import MaterialTable from "material-table";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import SearchBar from "material-ui-search-bar";
import Rodal from "rodal";
import { setAuthorizationBearer } from "../../requests/http";
import userContext from "../../contexts/userContext";
import "../scss/productAdmin.scss";

const drawerWidth = 280;

function rand() {
  return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    padding: theme.spacing(2),
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    padding: theme.spacing(2),
  },
}));
function getSteps() {
  return [
    "Information général",
    "Caractéristique du produit",
    "Media & ajouter promotion",
  ];
}

const ClippedDrawer = ({ location, protected: protectedProp }) => {
  const [formState, formActions] = useForm({
    initialValues: {
      label: "",
      category: "",
      subcategory: "",

      characteristics: {},
    },
    validation: {
      label: isStringEmpty,
    },
  });
  const { values, errors, touched } = formState;
  const { handleChange, setValues } = formActions;
  const { from } = decodeUri(location.search);
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const [addCategoryState, addCategory] = useApiState(addCategoryRequest);
  const [icon, setIcon] = useState();
  const [prod, setProd] = useState(false);
  const toggleProd = () => setProd(!prod);
  const [visible, setVisible] = useState(false);
  const [productId, setProductId] = useState("");
  const [productState, productCall] = useApiState(getProductRequest);
  const { user, setUser } = useContext(userContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const handleClos = () => {
    setAnchorEl(null);
  };
  const [selectedDate, setSelectedDate] = React.useState(
    new Date("2014-08-18T21:11:54")
  );
  const successtoast = () => {
    toast.success("vous avez ajouter une produit avec success", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
    hide();
  };
  const [promotion, setPromotion] = useState({
    datestart: new Date(),
    datefin: new Date(),
    reduction: 0,
  });
  const [open, setOpen] = React.useState(false);

  function resetProductValues() {
    setActiveStep(0);
    setValues({
      label: "",
      category: "",
      subcategory: "",
      characteristics: {},
      price: "",
      credit: "",
      quantity: "",
      description: "",
      marque: "",
    });
    setPromotion({
      datestart: new Date(),
      datefin: new Date(),
      reduction: 0,
    });
  }

  useEffect(() => {
    if (!visible) {
      resetProductValues();
    }
  }, [visible]);

  useEffect(() => {
    if (productId) {
      const product = productState.data?.find((p) => p._id === productId);
      const characteristics = {};
      product.characteristics.forEach((c) => {
        characteristics[c.label] = c.value;
      });
      setValues({
        ...product,
        subcategory: product.subcategory._id,
        category: product.category._id,
        characteristics,
      });
      setPromotion(product.promotion);
    } else {
      resetProductValues();
    }
  }, [productId]);

  const show = () => {
    setVisible(true);
  };
  const hide = () => {
    setVisible(false);
    setProductId("");
  };
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  const StyledTableRow = withStyles((theme) => ({
    root: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  const galleryImageList = [
    "https://raw.githubusercontent.com/dxyang/StyleTransfer/master/style_imgs/mosaic.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg/1280px-Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg",
    "https://raw.githubusercontent.com/ShafeenTejani/fast-style-transfer/master/examples/dora-maar-picasso.jpg",
    "https://pbs.twimg.com/profile_images/925531519858257920/IyYLHp-u_400x400.jpg",
    "https://raw.githubusercontent.com/ShafeenTejani/fast-style-transfer/master/examples/dog.jpg",
    "http://r.ddmcdn.com/s_f/o_1/cx_462/cy_245/cw_1349/ch_1349/w_720/APL/uploads/2015/06/caturday-shutterstock_149320799.jpg",
  ];
  const handleClose = () => {
    setOpen(false);
  };
  const [addProductState, addProduct] = useApiState(addProductRequest);
  const [updateProductState, updateProductCall] = useApiState(updateProduct);
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [caractéristique, setCaractéristique] = useState([""]);
  const [checked, setChecked] = React.useState(true);
  const [val, setVal] = useState([]);
  const [image, setImage] = useState();

  useDidMount(() => {
    categoryCall();
    subCall();
    productCall();
  });

  useEffect(() => {
    if (!addProductState.fetching && addProductState.data) {
      successtoast();
      productCall();
      setVisible(false);
    }
  }, [addProductState.fetching]);

  useEffect(() => {
    if (!updateProductState.fetching && updateProductState.data) {
      successtoast();
      productCall();
      setProductId("");
    }
  }, [updateProductState.fetching]);

  const onChange = (e, i) => {
    const nextCaractéristique = [...caractéristique];
    nextCaractéristique[i] = e.target.value;
    setCaractéristique(nextCaractéristique);
  };

  const selectedSubcategory =
    subState.data &&
    subState.data.find((sub) => sub._id === values.subcategory);

  useEffect(() => {
    if (values.subcategory) {
      const characteristics = {};
      selectedSubcategory.characteristics.forEach((car) => {
        characteristics[car.name] = car.options ? car.options[0] : "";
      });
      setValues({ characteristics });
    }
  }, [values.subcategory]);

  async function onSubmit(e) {
    e.preventDefault();

    const {
      label,
      price,
      category,
      subcategory,
      quantity,
      credit,
      marque,
      description,
    } = formState.values;
    const characteristics = Object.keys(values.characteristics).map((key) => ({
      label: key,
      value: values.characteristics[key],
    }));

    if (formActions.validateForm()) {
      const fn = productId ? updateProductCall : addProduct;
      const payload = {
        label,
        price,
        category,
        subcategory,
        quantity,
        credit,
        characteristics,
        marque,
        description,
        promotion: promotion,
      };
      if (image) {
        const url = await upload(image);
        payload.images = [url.url];
      }

      if (productId) payload.id = productId;
      fn(payload);
    } else {
      formActions.setAllTouched(true);
    }
    console.log(errors);
    console.log(formState);
  }

  console.log(values.characteristics, "ebay");

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <form onSubmit={onSubmit}>
            <div>
              {" "}
              <Row>
                <Col>
                  {" "}
                  <Form.Label className="text-lab">Label </Form.Label>
                  <Form.Control
                    type="text"
                    name="label"
                    placeholder="Label"
                    onChange={handleChange}
                    value={values.label}
                    className="lab-rotate"
                  />
                  {touched.label && errors.label && (
                    <div>
                      <ErrorIcon className="errorIcon" fontSize="small" />
                      <span className="errorMessage">{errors.label}</span>
                    </div>
                  )}
                </Col>
              </Row>
              <Form.Group className="prod-grp">
                <Row>
                  <Col>
                    <Form.Label className="text-lab">Quantité </Form.Label>
                    <Form.Control
                      type="number"
                      name="quantity"
                      placeholder="Quantité"
                      onChange={handleChange}
                      value={values.quantity}
                    />
                    {touched.quantity && errors.quantity && (
                      <div>
                        <ErrorIcon className="errorIcon" fontSize="small" />
                        <span className="errorMessage">{errors.quantity}</span>
                      </div>
                    )}
                  </Col>
                  <Col>
                    <Form.Label className="text-lab">Marque </Form.Label>
                    <Form.Control
                      type="text"
                      name="marque"
                      placeholder="Marque"
                      onChange={handleChange}
                      value={values.marque}
                    />
                    {touched.marque && errors.marque && (
                      <div>
                        <ErrorIcon className="errorIcon" fontSize="small" />
                        <span className="errorMessage">{errors.marque}</span>
                      </div>
                    )}
                  </Col>
                </Row>
                <Row className="rot-marg">
                  <Col>
                    <Form.Label className="text-lab">Price </Form.Label>
                    <Form.Control
                      type="number"
                      name="price"
                      placeholder="Price"
                      onChange={handleChange}
                      value={values.price}
                    />
                    {touched.price && errors.price && (
                      <div>
                        <ErrorIcon className="errorIcon" fontSize="small" />
                        <span className="errorMessage">{errors.price}</span>
                      </div>
                    )}
                  </Col>
                  <Col>
                    <Form.Label className="text-lab">Crédit </Form.Label>
                    <Form.Control
                      type="number"
                      name="credit"
                      placeholder="credit"
                      onChange={handleChange}
                      value={values.credit}
                    />
                    {touched.credit && errors.credit && (
                      <div>
                        <ErrorIcon className="errorIcon" fontSize="small" />
                        <span className="errorMessage">{errors.credit}</span>
                      </div>
                    )}
                  </Col>
                </Row>
              </Form.Group>
              <Row>
                <Col className="position-area">
                  <Form.Label className="text-lab">Description </Form.Label>

                  <TextareaAutosize
                    className="text-area"
                    rowsMin={4}
                    rowsMax={4}
                    type="text"
                    name="description"
                    placeholder="Description"
                    onChange={handleChange}
                    value={values.description}
                  />
                  {touched.description && errors.description && (
                    <div>
                      <ErrorIcon className="errorIcon" fontSize="small" />
                      <span className="errorMessage">{errors.description}</span>
                    </div>
                  )}
                </Col>
              </Row>
            </div>
          </form>
        );
      case 1:
        return (
          <form onSubmit={onSubmit}>
            <Form.Group className="formNom-prod">
              <Row>
                <Col>
                  <Form.Label className="text-lab">Catégorie </Form.Label>
                  <>
                    {" "}
                    <Form.Control
                      as="select"
                      onChange={handleChange}
                      value={values.category}
                      name="category"
                    >
                      <option value="">selectionne</option>
                      {categoryState.data &&
                        categoryState.data.map((row) => (
                          <option key={row._id} value={row._id}>
                            {row.label}{" "}
                          </option>
                        ))}{" "}
                      {touched.category && errors.category && (
                        <div>
                          <ErrorIcon className="errorIcon" fontSize="small" />
                          <span className="errorMessage">
                            {errors.category}
                          </span>
                        </div>
                      )}
                    </Form.Control>
                  </>{" "}
                </Col>

                <Col>
                  <Form.Group>
                    {" "}
                    <Form.Label className="text-lab">
                      Sous-categorie{" "}
                    </Form.Label>
                    <>
                      {" "}
                      <Form.Control
                        as="select"
                        onChange={handleChange}
                        value={values.subcategory}
                        name="subcategory"
                      >
                        <option value="">selectionne</option>
                        {subState.data &&
                          subState.data
                            .filter((sub) => values.category === sub.category)
                            .map((sub) => (
                              <option key={sub._id} value={sub._id}>
                                {sub.label}{" "}
                              </option>
                            ))}

                        {touched.subcategory && errors.subcategory && (
                          <div>
                            <ErrorIcon className="errorIcon" fontSize="small" />
                            <span className="errorMessage">
                              {errors.subcategory}
                            </span>
                          </div>
                        )}
                      </Form.Control>
                    </>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col className="down-input">
                  {" "}
                  {selectedSubcategory &&
                    selectedSubcategory.characteristics.map((sub) => (
                      <div className="container-inputs">
                        <p className="char-text">{sub.name} :</p>
                        <>
                          {" "}
                          <select
                            value={values.characteristics[sub.name] || ""}
                            onChange={(e) =>
                              setValues({
                                characteristics: {
                                  ...values.characteristics,
                                  [sub.name]: e.target.value,
                                },
                              })
                            }
                          >
                            {sub.options.map((option) => (
                              <option value={option}>{option}</option>
                            ))}
                          </select>
                        </>
                      </div>
                    ))}
                </Col>
              </Row>
            </Form.Group>
          </form>
        );
      case 2:
        return (
          <form onSubmit={onSubmit}>
            <Form.Group className="prod-grp">
              <Row>
                <Col className="imgandbtn">
                  <Form.Group className="last-prod-grp">
                    <center>Choisir une image:</center>
                    <ImageUpload
                      cardName="Input Image"
                      imageGallery={galleryImageList}
                      onChange={(event) => setImage(event.target.files[0])}
                    />
                    {touched.image && errors.images && (
                      <div>
                        <ErrorIcon className="errorIcon" fontSize="small" />
                        <span className="errorMessage">{errors.images}</span>
                      </div>
                    )}
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col className="promo-contai">
                  <div>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={checked}
                          onChange={(e) => setChecked(e.target.checked)}
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      }
                      label="Ajouter une promotion"
                    />
                  </div>

                  <div className="container-dates">
                    <Form.Control
                      type="number"
                      name="reduction"
                      placeholder="Reduction"
                      onChange={(e) =>
                        setPromotion({
                          ...promotion,
                          reduction: e.target.value,
                        })
                      }
                      className="promo-in"
                      value={promotion.reduction}
                    />
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        minDate={new Date()}
                        id="date-picker-inline"
                        label="Date de debut"
                        value={promotion.datestart}
                        className="promo-inputone "
                        onChange={(date) =>
                          setPromotion({ ...promotion, datestart: date })
                        }
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />

                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="MM/dd/yyyy"
                        margin="normal"
                        minDate={new Date()}
                        maxDate={new Date(2100, 0, 1)}
                        id="date-picker-inline"
                        label="Date de fin"
                        value={promotion.datefin}
                        className="promo-input"
                        onChange={(date) =>
                          setPromotion({ ...promotion, datefin: date })
                        }
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </div>
                  {touched.promotion && errors.promotion && (
                    <div>
                      <ErrorIcon className="errorIcon" fontSize="small" />
                      <span className="errorMessage">{errors.promotion}</span>
                    </div>
                  )}
                </Col>
              </Row>
            </Form.Group>
          </form>
        );
      default:
        return "Unknown stepIndex";
    }
  }

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {" "}
        <img src={Imageside} className="imgdash"></img>
        <div className="drawercontainer">
          <List>
            <Link to="/admin/home" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Accueil" />
              </ListItem>
            </Link>
            <Link to="/admin/categories" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <CategoryIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Categories" />
              </ListItem>
            </Link>
            <Link to="/admin/products" className="labelhome">
              <ListItem button className="actuallabelhome">
                <ListItemIcon>
                  <DevicesIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Produits" />
              </ListItem>
            </Link>
            <Link to="/admin/commandes" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <ShoppingCartIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Commandes" />
              </ListItem>
            </Link>
            <Link to="/admin/users" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <GroupIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Utilisateurs" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/reports" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <FlagIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Rapports" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/tickets" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <ConfirmationNumberIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText
                  primary="Tickets des utilisateurs"
                  className="labelhome"
                />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            {["Deconnexion"].map((text, index) => (
              <ListItem
                button
                key={text}
                onClick={() => {
                  localStorage.clear();
                  setAuthorizationBearer(null);
                  setUser(null);
                  handleClos();
                  if (!protectedProp) {
                    history.push("/login");
                  }
                }}
                className="labelhome"
              >
                <ListItemIcon>
                  <ExitToAppIcon className="labelhome" />{" "}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />{" "}
        <div>
          <Button
            variant="contained"
            onClick={show}
            color="secondary"
            className="btt-ad"
          >
            Ajouter produit
          </Button>
        </div>
        <Grid container spacing={1}>
          {/* <PlaylistAddIcon onClick={show} className="testicon-prod" /> */}
          <Grid item xs className="tab-ji">
            {" "}
            <MaterialTable
              title="Tableau des produits"
              localization={{
                body: {
                  emptyDataSourceMessage: (
                    <p>Aucun enregistrement à afficher</p>
                  ),
                },
                toolbar: {
                  searchPlaceholder: "Rechercher",
                },
                pagination: {
                  labelDisplayedRows: "Rangéés",
                  labelRowsSelect: "Rangéés",
                  previousTooltip: "page précédente",
                  nextTooltip: "page suivante",
                },
              }}
              columns={[
                {
                  title: "Image",
                  field: "imgUrl",
                  render: (rowData) => (
                    <img
                      src={rowData.imageUrl}
                      style={{ width: 40, borderRadius: "50%" }}
                    />
                  ),
                },
                {
                  title: "Label",
                  field: "label",
                  render: (rowData) => (
                    <p style={{ color: "blue" }}> {rowData.label}</p>
                  ),
                },

                {
                  title: "Categorie",
                  field: "category",
                },
                {
                  title: "Sous-categorie",
                  field: "subcategory",
                },
                {
                  title: "Prix en TND",
                  field: "price",
                },
                {
                  title: "Quantité",
                  field: "quantity",
                },
              ]}
              data={
                productState.data
                  ? productState.data.map((row, index) => ({
                      label: row.label,
                      imageUrl: row.images[0],
                      price: row.price,
                      quantity: row.quantity,
                      subcategory: row.subcategory.label,
                      category: row.category.label,
                      id: row._id,
                    }))
                  : []
              }
              actions={[
                {
                  icon: "edit",
                  tooltip: "Modifier produit",
                  onClick: (event, rowData) => {
                    setProductId(rowData.id);
                  },
                },
                {
                  icon: "delete",
                  tooltip: "Delete User",
                  onClick: (event, rowData) =>
                    deleteProduit({ id: rowData.id }).then(() => {
                      productCall();
                    }),
                },
              ]}
              options={({ search: false }, { pageSize: 10 })}
            />
          </Grid>
        </Grid>
        <Rodal
          className="test-rota"
          animation="door"
          duration={800}
          visible={visible || productId}
          onClose={hide}
          width={900}
          height={600}
          closeOnEsc={true}
        >
          <div className="modal-rotate">
            {" "}
            <Stepper activeStep={activeStep} alternativeLabel>
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <div>
              {activeStep === steps.length ? (
                <div>
                  <Typography className={classes.instructions}>
                    All steps completed
                  </Typography>
                  <Button onClick={handleReset}>Reset</Button>
                </div>
              ) : (
                <div>
                  <div className={classes.instructions}>
                    {getStepContent(activeStep)}
                  </div>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.backButton}
                      className="bot-back"
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={
                        activeStep === steps.length - 1 ? onSubmit : handleNext
                      }
                      className="bot-btn"
                    >
                      {activeStep === steps.length - 1 ? "Finish" : "Next"}
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </Rodal>
      </main>
    </div>
  );
};
export default ClippedDrawer;
