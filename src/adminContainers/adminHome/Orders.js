import React, { useContext, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";

import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import DevicesIcon from "@material-ui/icons/Devices";
import GroupIcon from "@material-ui/icons/Group";
import { Link, withRouter, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import HomeFragments from "../homefragments/homeFragments";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import Imageside from "../adminAssets/sidebar-4.jpg";
import { useDidMount } from "../../hooks/useLifeCycle";
import { getAllorders, updateOrder } from "../../requests/order";
import TND from "../adminAssets/tnd.png";
import useApiState from "../../hooks/useApiState";
import Chart from "./chart.js";
import MaterialTable from "material-table";
import FlagIcon from "@material-ui/icons/Flag";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Moment from "react-moment";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import EditIcon from "@material-ui/icons/Edit";
import { getorder, deleteOrder } from "../../requests/order";
import { setAuthorizationBearer } from "../../requests/http";
import userContext from "../../contexts/userContext";
import Rodal from "rodal";

import "../scss/order.scss";

const drawerWidth = 280;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function ClippedDrawer({ protected: protectedProp }) {
  const classes = useStyles();
  const { user, setUser } = useContext(userContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const handleClos = () => {
    setAnchorEl(null);
  };
  let test = 0;
  const [ordersState, ordersCall] = useApiState(getAllorders);
  const [visible, setVisible] = useState(false);
  const [orderState, orderCall] = useApiState(getorder);

  const [updateOrderState, updateOrderCall] = useApiState(updateOrder);
  useEffect(() => {
    if (!updateOrderState.fetching && updateOrderState.data) {
      ordersCall();
    }
  }, [updateOrderState.fetching]);
  const show = (id) => {
    setVisible(id);
  };
  const hide = () => {
    setVisible(false);
  };

  useDidMount(() => {
    ordersCall();
  });

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {" "}
        <img src={Imageside} className="imgdash"></img>
        <div className="drawercontainer">
          <List>
            <Link to="/admin/home" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Accueil" />
              </ListItem>
            </Link>
            <Link to="/admin/categories" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <CategoryIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Categories" />
              </ListItem>
            </Link>
            <Link to="/admin/products" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <DevicesIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Produits" />
              </ListItem>
            </Link>
            <Link to="/admin/commandes" className="labelhome">
              <ListItem button className="actuallabelhome">
                <ListItemIcon>
                  <ShoppingCartIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Commandes" />
              </ListItem>
            </Link>
            <Link to="/admin/users" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <GroupIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Utilisateurs" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/reports" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <FlagIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Rapports" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/tickets" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <ConfirmationNumberIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText
                  primary="Tickets des utilisateurs"
                  className="labelhome"
                />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            {["Deconnexion"].map((text, index) => (
              <ListItem
                button
                key={text}
                onClick={() => {
                  localStorage.clear();
                  setAuthorizationBearer(null);
                  setUser(null);
                  handleClos();
                  if (!protectedProp) {
                    history.push("/login");
                  }
                }}
                className="labelhome"
              >
                <ListItemIcon>
                  <ExitToAppIcon className="labelhome" />{" "}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Typography>
          <div className="traite-t">Tableau des orders non traités</div>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className="labels-order">N°.</TableCell>
                  <TableCell align="center" className="labels-order">
                    Id Order
                  </TableCell>{" "}
                  <TableCell align="center" className="labels-order">
                    Client
                  </TableCell>
                  <TableCell align="center" className="labels-order">
                    Products
                  </TableCell>
                  <TableCell align="center" className="labels-order">
                    Date
                  </TableCell>
                  <TableCell align="center" className="labels-order">
                    Prix en TND
                  </TableCell>
                  <TableCell align="left" className="labels-status">
                    Status
                  </TableCell>
                  <TableCell align="center" className="labels-order">
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ordersState.data &&
                  ordersState.data
                    .filter((item) => item.status === "not processed")
                    .map((row, index) => {
                      return (
                        <TableRow key={row._id}>
                          <Rodal
                            className="rota-order"
                            animation="slideRight"
                            duration={800}
                            visible={row._id === visible}
                            onClose={hide}
                            width={300}
                            height="auto"
                            overflow="auto"
                            closeOnEsc={true}
                          >
                            <div className="dytra">
                              {row.products.map((p) => {
                                return (
                                
                                    <div className="plz-dis">
                                      {" "}
                                      <img
                                        src={p.product.images[0]}
                                        className="img-day"
                                      />{" "}
                                      <center className="tit-rodl">
                                        {p.product.label}
                                      </center>
                                    </div>
                                    
                                
                                );
                              })}
                            </div>
                          </Rodal>
                          <TableCell component="th" scope="row">
                            {index + 1}
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            {row._id.substring(18, 24)}
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            {row.user?.firstName} {row.user?.lastName}
                          </TableCell>
                          <TableCell align="center" className="cons-position">
                            {" "}
                            <Button
                              variant="outlined"
                              size="small"
                              color="primary"
                              onClick={() => show(row._id)}
                            >
                              Voir les produits
                            </Button>
                          </TableCell>
                          <TableCell align="center">
                            {" "}
                            <Moment format="YYYY/MM/DD">{row.date}</Moment>
                          </TableCell>{" "}
                          <TableCell align="center"> {row.price}</TableCell>
                          <TableCell align="center" className="status-position">
                            {row.status === "not processed" ? (
                              <div className="w3-containerbad">
                                <div className="w3-border">
                                  <div className="w3-grey"></div>
                                </div>
                                <div className="w3-circle"></div>
                                <div className="w3-text">non traité</div>
                              </div>
                            ) : (
                              <div className="w3-container">
                                <div className="w3-border">
                                  <div className="w3-greysuccess"></div>
                                </div>
                                <div className="w3-circlesuccess"></div>
                                <div className="w3-text"> {row.status}</div>
                              </div>
                            )}
                          </TableCell>
                          <TableCell align="center" className="del-position">
                            {row.status === "not processed" ? (
                              <div className="ides-tool">
                                <Tooltip
                                  title="Supprimer"
                                  placement="left"
                                  className="order-delt"
                                >
                                  <IconButton aria-label="delete">
                                    <DeleteIcon
                                      onClick={() =>
                                        deleteOrder({ id: row._id }).then(
                                          () => {
                                            ordersCall();
                                          }
                                        )
                                      }
                                    />{" "}
                                  </IconButton>
                                </Tooltip>
                                <Tooltip
                                  title="Accepter"
                                  placement="top"
                                  className="order-delt"
                                >
                                  <IconButton aria-label="delete">
                                    <CheckCircleOutlineIcon
                                      onClick={() => {
                                        updateOrderCall({
                                          id: row._id,
                                          status: "processed",
                                        });
                                      }}
                                    />
                                  </IconButton>
                                </Tooltip>
                              </div>
                            ) : (
                              <div></div>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
              </TableBody>
            </Table>
          </TableContainer>{" "}
          <div className="notproces">
            {" "}
            <div className="traite-t">Tableau des orders traités</div>
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className="labels-order">N°.</TableCell>
                    <TableCell align="center" className="labels-order">
                      Id Order
                    </TableCell>{" "}
                    <TableCell align="center" className="labels-order">
                      Client
                    </TableCell>
                    <TableCell align="center" className="labels-order">
                      Products
                    </TableCell>
                    <TableCell align="center" className="labels-order">
                      Date
                    </TableCell>{" "}
                    <TableCell align="center" className="labels-order">
                      Prix en TND
                    </TableCell>
                    <TableCell align="left" className="labels-status">
                      Status
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {ordersState.data &&
                    ordersState.data
                      .filter((item) => item.status === "processed")
                      .map((row, index) => (
                        <TableRow key={row._id}>
                          <Rodal
                            className="rota-order"
                            animation="slideRight"
                            duration={800}
                            visible={row._id === visible}
                            onClose={hide}
                            width={300}
                            height="auto"
                            overflow="auto"
                            closeOnEsc={true}
                          >
                            <div className="dytra">
                              {row.products.map((p) => {
                                return (
                                  <>
                                    <div className="plz-dis">
                                      {" "}
                                      <img
                                        src={p.product.images[0]}
                                        className="img-day"
                                      />{" "}
                                      <center className="tit-rodl">
                                        {p.product.label}
                                      </center>
                                    </div>
                                    <div></div>
                                  </>
                                );
                              })}
                            </div>
                          </Rodal>
                          <TableCell component="th" scope="row">
                            {index + 1}
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            {row._id.substring(18, 24)}
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            {row.user?.firstName} {row.user?.lastName}
                          </TableCell>
                          <TableCell align="center" className="cons-position">
                            {" "}
                            <Button
                              variant="outlined"
                              size="small"
                              color="primary"
                              onClick={() => show(row._id)}
                            >
                              Voir les produits
                            </Button>
                          </TableCell>
                          <TableCell align="center">
                            {" "}
                            <Moment format="YYYY/MM/DD">{row.date}</Moment>
                          </TableCell>{" "}
                          <TableCell align="center"> {row.price}</TableCell>
                          <TableCell align="center" className="status-position">
                            {row.status === "not processed" ? (
                              <div className="w3-containerbad">
                                <div className="w3-border">
                                  <div className="w3-grey"></div>
                                </div>
                                <div className="w3-circle"></div>
                                <div className="w3-text"> Non traité</div>
                              </div>
                            ) : (
                              <div className="w3-container">
                                <div className="w3-border">
                                  <div className="w3-greysuccess"></div>
                                </div>
                                <div className="w3-circlesuccess"></div>
                                <div className="w3-text">Traité</div>
                              </div>
                            )}
                          </TableCell>
                          <TableCell align="center" className="del-position">
                            {row.status === "not processed" ? (
                              <div className="ides-tool">
                                <Tooltip
                                  title="Supprimer"
                                  placement="left"
                                  className="order-delt"
                                >
                                  <IconButton aria-label="delete">
                                    <DeleteIcon />
                                  </IconButton>
                                </Tooltip>
                                <Tooltip
                                  title="Accepter"
                                  placement="top"
                                  className="order-delt"
                                >
                                  <IconButton aria-label="delete">
                                    <CheckCircleOutlineIcon
                                      onClick={() => {
                                        updateOrderCall({
                                          id: row._id,
                                          status: "processed",
                                        });
                                      }}
                                    />
                                  </IconButton>
                                </Tooltip>
                              </div>
                            ) : (
                              <div></div>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </Typography>
      </main>
    </div>
  );
}
