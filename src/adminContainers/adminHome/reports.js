import React, { useContext, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FlagIcon from "@material-ui/icons/Flag";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import DevicesIcon from "@material-ui/icons/Devices";
import GroupIcon from "@material-ui/icons/Group";
import { Link, withRouter, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import HomeFragments from "../homefragments/homeFragments";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import Imageside from "../adminAssets/sidebar-4.jpg";
import { useDidMount } from "../../hooks/useLifeCycle";
import { getUsers } from "../../requests/auth";
import TND from "../adminAssets/tnd.png";
import useApiState from "../../hooks/useApiState";
import Chart from "./chart.js";
import ComponentDrawer from "./drawer";
import { setAuthorizationBearer } from "../../requests/http";
import userContext from "../../contexts/userContext";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { getContact } from "../../requests/assistance";

import "../scss/admin.scss";
import { getReport } from "../../requests/report";
import { deleteReview } from "../../requests/product";

const drawerWidth = 280;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function ClippedDrawer({ protected: protectedProp }) {
  const classes = useStyles();
  const [reportState, reportCall] = useApiState(getReport);
  const [deleteReviewState, deleteReviewCall] = useApiState(deleteReview);

  useDidMount(() => {
    reportCall();
  });
  const { user, setUser } = useContext(userContext);
  const [anchorEl, setAnchorEl] = useState(null);

  const history = useHistory();

  useEffect(() => {
    if (!deleteReviewState.fetching && !deleteReviewState.error) {
      reportCall();
    }
  }, [deleteReviewState.fetching]);

  const handleClos = () => {
    setAnchorEl(null);
  };
  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {" "}
        <img src={Imageside} className="imgdash"></img>
        <div className="drawercontainer">
          <List>
            <Link to="/admin/home" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Accueil" />
              </ListItem>
            </Link>
            <Link to="/admin/categories" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <CategoryIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Categories" />
              </ListItem>
            </Link>
            <Link to="/admin/products" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <DevicesIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Produits" />
              </ListItem>
            </Link>
            <Link to="/admin/commandes" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <ShoppingCartIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Commandes" />
              </ListItem>
            </Link>
            <Link to="/admin/users" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <GroupIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Utilisateurs" className="labelhome" />
              </ListItem>
            </Link>{" "}
            <Link to="/admin/reports" className="labelhome">
              <ListItem button className="actuallabelhome">
                <ListItemIcon>
                  <FlagIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Rapports" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/tickets" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <ConfirmationNumberIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText
                  primary="Tickets des utilisateurs"
                  className="labelhome"
                />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            {["Deconnexion"].map((text, index) => (
              <ListItem
                button
                key={text}
                onClick={() => {
                  localStorage.clear();
                  setAuthorizationBearer(null);
                  setUser(null);
                  handleClos();
                  if (!protectedProp) {
                    history.push("/login");
                  }
                }}
                className="labelhome"
              >
                <ListItemIcon>
                  <ExitToAppIcon className="labelhome" />{" "}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Typography>
          <Grid container spacing={3}>
            <Grid item xs>
              <TableContainer component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell className="labels-order">N°.</TableCell>{" "}
                      <TableCell align="center" className="labels-order">
                        Client
                      </TableCell>
                      <TableCell align="center" className="labels-order">
                        Commentaire
                      </TableCell>
                      <TableCell align="center" className="labels-order">
                        Type
                      </TableCell>
                      <TableCell align="center" className="labels-order">
                        Reason
                      </TableCell>
                      <TableCell align="center" className="labels-order">
                        Action
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {reportState.data &&
                      reportState.data.map((row, index) => (
                        <TableRow key={row._id}>
                          <TableCell component="th" scope="row">
                            {index + 1}
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            <div>
                              {row.user.firstName} {row.user.firstName}
                            </div>
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            {" "}
                            <div>{row.review?.review}</div>
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            <div>{row.type}</div>
                          </TableCell>
                          <TableCell align="center" className="id-position">
                            <div>{row.reason || "----------"}</div>
                          </TableCell>

                          <TableCell align="center">
                            {" "}
                            <Tooltip
                              title="Supprimer"
                              placement="left"
                              className="order-delt"
                            >
                              <IconButton aria-label="delete">
                                <DeleteIcon
                                  onClick={() =>
                                    deleteReviewCall({ id: row.review?._id })
                                  }
                                />
                              </IconButton>
                            </Tooltip>
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Typography>
      </main>
    </div>
  );
}
