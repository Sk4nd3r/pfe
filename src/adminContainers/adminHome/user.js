import React, { useContext, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";

import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import DevicesIcon from "@material-ui/icons/Devices";
import GroupIcon from "@material-ui/icons/Group";
import { Link, withRouter, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import HomeFragments from "../homefragments/homeFragments";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import Imageside from "../adminAssets/sidebar-4.jpg";
import { useDidMount } from "../../hooks/useLifeCycle";
import { getAllorders, updateOrder } from "../../requests/order";
import { getUsers } from "../../requests/auth";

import TND from "../adminAssets/tnd.png";
import useApiState from "../../hooks/useApiState";
import Chart from "./chart.js";
import MaterialTable from "material-table";
import FlagIcon from "@material-ui/icons/Flag";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Moment from "react-moment";
import EditIcon from "@material-ui/icons/Edit";
import Rodal from "rodal";
import ComponentDrawer from "./drawer";
import { setAuthorizationBearer } from "../../requests/http";
import userContext from "../../contexts/userContext";
import "../scss/admin.scss";

const drawerWidth = 280;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function ClippedDrawer({ protected: protectedProp }) {
  const classes = useStyles();
  const [usersState, usersCall] = useApiState(getUsers);
  const [hex, setHex] = useState("#ffffff");
  const { user, setUser } = useContext(userContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const handleClos = () => {
    setAnchorEl(null);
  };
  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  useDidMount(() => {
    usersCall();
  });
  console.log(usersState.data, "okok");
  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {" "}
        <img src={Imageside} className="imgdash"></img>
        <div className="drawercontainer">
          <List>
            <Link to="/admin/home" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Accueil" />
              </ListItem>
            </Link>
            <Link to="/admin/categories" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <CategoryIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Categories" />
              </ListItem>
            </Link>
            <Link to="/admin/products" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <DevicesIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Produits" />
              </ListItem>
            </Link>
            <Link to="/admin/commandes" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <ShoppingCartIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Commandes" />
              </ListItem>
            </Link>
            <Link to="/admin/users" className="labelhome">
              <ListItem button className="actuallabelhome">
                <ListItemIcon>
                  <GroupIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Utilisateurs" className="labelhome" />
              </ListItem>
            </Link>{" "}
            <Link to="/admin/reports" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <FlagIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Rapports" className="labelhome" />
              </ListItem>
            </Link>
            <Link to="/admin/tickets" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <ConfirmationNumberIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText
                  primary="Tickets des utilisateurs"
                  className="labelhome"
                />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            {["Deconnexion"].map((text, index) => (
              <ListItem
                button
                key={text}
                onClick={() => {
                  localStorage.clear();
                  setAuthorizationBearer(null);
                  setUser(null);
                  handleClos();
                  if (!protectedProp) {
                    history.push("/login");
                  }
                }}
                className="labelhome"
              >
                <ListItemIcon>
                  <ExitToAppIcon className="labelhome" />{" "}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Typography>
          <MaterialTable
          localization={{
            body: {
              emptyDataSourceMessage: (
                <p>Aucun enregistrement à afficher</p>
              ),
            },
            toolbar: {
              searchPlaceholder: "Rechercher"
            },
            pagination: {
              labelDisplayedRows: "Rangéés",
              labelRowsSelect:"Rangéés",
              previousTooltip:"page précédente",
              nextTooltip:"page suivante",
              

            },
          }}
            title={"Admin Table"}
            columns={[
              {
                title: "id_Admin",
                field: "id",
              },

              {
                title: "Nom",
                field: "firstName",
              },
              {
                title: "Prenom",
                field: "lastName",
              },
              {
                title: "email",
                field: "email",
              },
            ]}
            data={
              usersState.data
                ? usersState.data
                    .filter((user) => user.role === "admin")
                    .map((row, index) => ({
                      index: row.firstName.substring(0, 1),
                      id: row._id.substring(18, 24),
                      firstName: row.firstName,
                      lastName: row.lastName,
                      email: row.email,
                      points: row.points,
                    }))
                : []
            }
            actions={[
              {
                hidden: "true",
                icon: "add",
                tooltip: "Add User",
                isFreeAction: true,
                onClick: (event) => alert("You want to add a new row"),
              },
            ]}
            options={({ search: false }, { pageSize: 5 })}
          />
          <div className="seprat"></div>
          <MaterialTable
            title={"Clients Table"}
            localization={{
              body: {
                emptyDataSourceMessage: <p>Aucun enregistrement à afficher</p>,
              },
              toolbar: {
                searchPlaceholder: "Rechercher",
              },
              pagination: {
                labelDisplayedRows: "Rangéés",
                labelRowsSelect: "Rangéés",
                previousTooltip: "page précédente",
                nextTooltip: "page suivante",
              },
            }}
            columns={[
              {
                title: "id_Client",
                field: "id",
              },

              {
                title: "Nom",
                field: "firstName",
              },
              {
                title: "Prenom",
                field: "lastName",
              },
              {
                title: "email",
                field: "email",
              },
              {
                title: "Points",
                field: "points",
              },
            ]}
            data={
              usersState.data
                ? usersState.data
                    .filter((user) => user.role === "user")
                    .map((row, index) => ({
                      index: row.firstName.substring(0, 1),
                      id: row._id.substring(18, 24),
                      firstName: row.firstName,
                      lastName: row.lastName,
                      email: row.email,
                      points: row.points,
                    }))
                : []
            }
            actions={[
              {
                hidden: "true",
                icon: "add",
                tooltip: "Add User",
                isFreeAction: true,
                onClick: (event) => alert("You want to add a new row"),
              },
            ]}
            options={({ search: false }, { pageSize: 5 })}
          />
        </Typography>
      </main>
    </div>
  );
}
