import React, { useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import DevicesIcon from "@material-ui/icons/Devices";
import GroupIcon from "@material-ui/icons/Group";
import { Link, withRouter, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import HomeFragments from "../homefragments/homeFragments";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import Imageside from "../adminAssets/sidebar-4.jpg";
import Modal from "@material-ui/core/Modal";
import { useForm } from "../../hooks/useInputs";
import { isStringEmpty, isNumberEmpty } from "../../utils/validation";
import { upload } from "../../requests/categories";
import { addCategoryRequest } from "../../requests/categories";
import { useDidMount } from "../../hooks/useLifeCycle";
import useApiState from "../../hooks/useApiState";
import { decodeUri } from "../../utils/url";
import TextField from "@material-ui/core/TextField";
import FilledInput from "@material-ui/core/FilledInput";
import { Card, Col, Row, Form, Carousel, Dropdown } from "react-bootstrap";
import Button from "@material-ui/core/Button/Button";
import ImageUpload from "../imageUpload";
import ErrorIcon from "@material-ui/icons/Error";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { getCategories } from "../../requests/categories";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import ComponentDrawer from "./drawer";

import "../scss/categoriesAdmin.scss";

const drawerWidth = 280;

function rand() {
  return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  table: {
    minWidth: 700,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    padding: theme.spacing(2),
  },
}));

const ClippedDrawer = ({ location }) => {
  const [formState, formActions] = useForm({
    initialValues: {
      label: "",
    },
    validation: {
      label: isStringEmpty,
    },
  });
  const [subCategoryParent,setSubCategoryParent]=useState("");
  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const [addCategoryState, addCategory] = useApiState(addCategoryRequest);

  const [flip, setFlipped] = useState(false);
  const toggleFlipped = () => setFlipped(!flip);

  const [image, setImage] = useState();
  const [icon, setIcon] = useState();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const galleryImageList = [
    "https://raw.githubusercontent.com/dxyang/StyleTransfer/master/style_imgs/mosaic.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg/1280px-Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg",
    "https://raw.githubusercontent.com/ShafeenTejani/fast-style-transfer/master/examples/dora-maar-picasso.jpg",
    "https://pbs.twimg.com/profile_images/925531519858257920/IyYLHp-u_400x400.jpg",
    "https://raw.githubusercontent.com/ShafeenTejani/fast-style-transfer/master/examples/dog.jpg",
    "http://r.ddmcdn.com/s_f/o_1/cx_462/cy_245/cw_1349/ch_1349/w_720/APL/uploads/2015/06/caturday-shutterstock_149320799.jpg",
  ];
  const handleClose = () => {
    setOpen(false);
  };
  const [categoryState, categoryCall] = useApiState(getCategories);
  useDidMount(() => {
    categoryCall();

  });
  console.log(categoryState)
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  function addsuBCategory(id){
    setSubCategoryParent(id);
    setOpen(true);
  }

  
  async function onSubmit(e) {
    e.preventDefault();
    const url = await upload(image);
    const urlicon = await upload(icon);

    const { label } = formState.values;

    if (formActions.validateForm()) {
      addCategory({
        label,
        image: url.url,
        icon: urlicon.url,
        parent:subCategoryParent || null
      });
    } else {
      formActions.setAllTouched(true);
    }
    console.log(url);
    console.log(errors);
    console.log(addCategoryState);

  }
  console.log(subCategoryParent)
  const body = (
    <div
      style={modalStyle}
      className={toggleFlipped ? "modalcontainer" : "modFlipped"}
    >
      {/* 
      
      */}
      <Grid container className="trythis">
        <Form onSubmit={onSubmit} className="formimg">
          <Grid item xs={12} className="titcontainer">
            <h1 className="familytitle" onClick={() => toggleFlipped}>
              Ajout categorie
            </h1>{" "}
          </Grid>
          <Grid item xs={12} className="titcontainer">
            <Form.Control
              type="text"
              name="label"
              placeholder="Label"
              onChange={handleChange}
              value={values.label}
              className="chooselabel"
            />
            {touched.label && errors.label && (
              <div>
                <ErrorIcon className="errorIcon" fontSize="small" />
                <span className="errorMessage">{errors.label}</span>
              </div>
            )}
          </Grid>
          <Form.Group>
            <Form.File
              id="exampleFormControlFile1"
              onChange={(event) => setImage(event.target.files[0])}
              className="imgmarg"
            />
          </Form.Group>
          {/*      <Form.Group>
            <ImageUpload
              id="exampleFormControlFile1"
              cardName="Input Image"
              imageGallery={galleryImageList}
              onChange={(event) => setImage(event.target.files[0])}
            />
          </Form.Group>
 */}
          <Grid item xs={6}>
            <Form.Group>
              <Form.File
                id="exampleFormControlFile1"
                onChange={(e) => setIcon(e.target.files[0])}
                className="imgmarg"
              />
            </Form.Group>{" "}
            *
          </Grid>
          <Button variant="outlined" className="addproductbtn" type="submit">
            Submit
          </Button>
        </Form>
      </Grid>
      <Grid container clasName="backFlip">
        <p>Hello world</p>
      </Grid>
    </div>
  );

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {" "}
        <img src={Imageside} className="imgdash"></img>
        <div className="drawercontainer">
          <List>
            <Link to="/admin/home" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Home" />
              </ListItem>
            </Link>
            <Link to="/admin/categories" className="labelhome">
              <ListItem button className="actuallabelhome">
                <ListItemIcon>
                  <CategoryIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Categories" />
              </ListItem>
            </Link>
            <Link to="/admin/products" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <DevicesIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Produits" />
              </ListItem>
            </Link>
            <Link to="/" className="labelhome">
              <ListItem button>
                <ListItemIcon>
                  <ShoppingCartIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Commandes" />
              </ListItem>
            </Link>
            <Link to="/" className="labelhome">
              <ListItem button className="labelhome">
                <ListItemIcon>
                  <GroupIcon className="labelhome" />
                </ListItemIcon>
                <ListItemText primary="Users" className="labelhome" />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            {["Deconnexion"].map((text, index) => (
              <ListItem button key={text} className="labelhome">
                <ListItemIcon>
                  <ExitToAppIcon className="labelhome" />{" "}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Typography>
          <Grid container className="bothcardContainer" spacing={2}>
            <Grid item xs>
              <Paper className="consultContainer">
                <div className="consult">
                  <PlaylistAddIcon onClick={handleOpen} className="testicon" />
                </div>
                <div className="contentTest">
                  <h3>Add Categorie</h3>
                  <p>
                    Lorem ipsum Lorem ipsum Lorem ipsumLorem ipsumLorem ipsum
                  </p>
                </div>
                <Modal
                  open={open}
                  onClose={handleClose}
                  aria-labelledby="simple-modal-title"
                  aria-describedby="simple-modal-description"
                >
                  {body}
                </Modal>
              </Paper>{" "}
            </Grid>
            <Grid item xs>
              <Paper className="consultContainerthree">
                <div className="consultthree">
                  <CategoryIcon className="testiconthree" />
                </div>
                <div className="contentTestthree">
                  <h3>Consulter Categories</h3>
                  <p>
                    Lorem ipsum Lorem ipsum Lorem ipsumLorem ipsumLorem ipsum
                  </p>
                </div>
              </Paper>{" "}
            </Grid>
          </Grid>
          <Grid container className="tabcontain">
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell>Nom</StyledTableCell>
                    <StyledTableCell align="right">Image</StyledTableCell>
                    <StyledTableCell align="right">
                    Icon
                    </StyledTableCell>
                    <StyledTableCell align="right">
                     Modifier
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      Add
                    </StyledTableCell>
                    <StyledTableCell align="right">
                      Supprimer
                    </StyledTableCell>
                  
                  </TableRow>
                </TableHead>
                <TableBody>
                  {categoryState.data &&
              categoryState.data.map((row) => (
                    <StyledTableRow key={row._id}>
                      <StyledTableCell component="th" scope="row">
                        {row.label}
                      </StyledTableCell>
                      <StyledTableCell align="right">
                        k
                      </StyledTableCell>
                      <StyledTableCell align="right">kz</StyledTableCell>
                      <StyledTableCell align="right">
                        <EditIcon className="edicon"/>
                      </StyledTableCell>
                      <StyledTableCell align="right">
                       <DeleteForeverIcon onClick={()=>addsuBCategory(row._id) } className="delicon"/>
                      </StyledTableCell>
                      <StyledTableCell align="right">
                       <DeleteForeverIcon className="delicon"/>
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Typography>
      </main>
    </div>
  );
};
export default ClippedDrawer;
