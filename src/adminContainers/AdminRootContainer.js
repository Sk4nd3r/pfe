import React, { useState, useContext } from "react";
import { Route, Switch, useHistory } from "react-router-dom";
import UserContext from "../contexts/userContext";
// hoc
// containers
import AdminLoginContainer from "./AdminLoginContainer";
import AdminHomeContainer from "./AdminHomeContainer";
import AdminHome from "./adminHome/Admin";
import CategoriesAdmin from "./adminHome/categoriesAdmin";
import SubCategoriesAdmin from "./adminHome/subCategoriesAdmin";
import Orders from "./adminHome/Orders.js";
import User from "./adminHome/user.js";
import Reports from "./adminHome/reports.js";
import Tickets from "./adminHome/tickets.js";

import ProductsAdmin from "./adminHome/productsAdmin";

// styles
import classNames from "../utils/classNames";
// hooks

import { useDidMount } from "../hooks/useLifeCycle";
// requests
import { refreshRequest } from "../requests/auth";
import "./scss/root.scss";
import { setAuthorizationBearer } from "../requests/http";
const AdminRootContainer = () => {
  const { user, setUser } = useContext(UserContext);
  const [startup, setStartup] = useState(false);
  const history = useHistory();
  /*
   async function onStart() {
    try {
      const t = localStorage.getItem('token');
      if (t) {
        const data = JSON.parse(t);
        const tokenResponse = await refreshRequest({ email: data.user.email, refreshToken: data.token.refreshToken });
        if (tokenResponse.status === 'OK') {
          console.log(data);
          setAuthorizationBearer(tokenResponse.data.accessToken);
          localStorage.setItem('token', JSON.stringify({ user: data.user, token: tokenResponse.data }));
          setUser(data.user);
        }
      }
      setStartup(true);
    } catch (e) {
      setStartup(true);
    }
  } 
   useDidMount(() => {
    onStart().then((nextUser) => {
      if (nextUser) setUser(nextUser);
      setStartup(true);
    });

    history.listen(() => {
      window.scrollTo({ top: 0, left: 0 });
    });
  
  });
  if (!startup) return <div />; */

  if (!user || user.role === "user") {
    return <div>404 Not found</div>;
  }
  return (
    <Switch>
      <Route path="/admin/adminLogin" component={AdminLoginContainer} />
      <Route path="/admin/adminHome" component={AdminHomeContainer} />
      <Route path="/admin/home" component={AdminHome} />
      <Route path="/admin/categories" component={CategoriesAdmin} />
      <Route path="/admin/listcategories" component={SubCategoriesAdmin} />
      <Route path="/admin/commandes" component={Orders} />
      <Route path="/admin/products" component={ProductsAdmin} />{" "}
      <Route path="/admin/users" component={User} />{" "}
      <Route path="/admin/reports" component={Reports} />{" "}
      <Route path="/admin/tickets" component={Tickets} />
    </Switch>
  );
};
export default AdminRootContainer;
