import { createContext } from "react";

export default createContext({
  getCartState: {},
  getCartCall: () => {},
});
