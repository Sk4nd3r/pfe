import { createContext } from "react";

export default createContext({
    getReviewsState: {},
    getReviewsCall: () => {},
});
