import React, { useEffect, useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import useApiState from "../hooks/useApiState";
import { getReply, addReply } from "../requests/reply";
import userContext from "../contexts/userContext";
import { TextField } from "@material-ui/core";
import Send from "@material-ui/icons/Send";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: 60,
    width: "100%",
  },
  title: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#212121",
    cursor: "pointer",
  },
  replyContainer: {
    paddingLeft: 20,
    width: "100%",
    padding: "15px 0",
    fontSize: 12,
    lineHeight: 1,
  },
  userName: {
    fontWeight: "bold",
  },
  text: {
    marginTop: 4,
    marginLeft: 12,
  },
  input: {
    width: "40vw",
  },
  icon: {
    cursor: "pointer",
  },
});

const Replies = ({ id }) => {
  const classes = useStyles();
  const { user } = useContext(userContext);
  const [getReplyState, getReplyCall] = useApiState(getReply);
  const [addReplyState, addReplyCall] = useApiState(addReply);
  const [reply, setReply] = useState("");

  useEffect(() => {
    getReplyCall({ review: id });
  }, [id, addReplyState.data]);

  function handleSubmit() {
    addReplyCall({ id, reply });
  }
  console.log(addReplyState.data);
  if (!getReplyState.data) return <div />;

  return (
    <div className={classes.container}>
      <div className={classes.title}>réponse</div>

      {getReplyState.data?.map((reply) => (
        <div className={classes.replyContainer}>
          <div className={classes.userName}>
            {reply.user.firstName} {reply.user.lastName}
          </div>
          <div className={classes.text}>{reply.reply}</div>
        </div>
      ))}

      {!getReplyState.data?.find((r) => r.user._id === user?._id) && (
        <TextField
          className={classes.input}
          value={reply}
          onChange={(e) => setReply(e.target.value)}
          InputProps={{
            endAdornment: (
              <Send className={classes.icon} onClick={handleSubmit} />
            ),
          }}
        />
      )}
    </div>
  );
};

export default Replies;
