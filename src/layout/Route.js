import React, { useContext } from 'react';

import { Redirect, Route as BaseRoute, RouteProps } from 'react-router-dom';

import userContext from '../contexts/userContext';
import Header from './Header';
import SideBar from './SideBar';
import Footer from './Footer';
import {Form, Button , FormControl, Nav,NavDropdown ,Col , Container, Row } from 'react-bootstrap';

import classNames from '../utils/classNames';
import { encodeUri } from '../utils/url';
import Categories from './Categories';



const Route = ({
 protected: protectedProp, addFooter, addHeader, addSideBar,addCategories, ...props
}) => {
  const { user } = useContext(userContext);
  console.log(props.path);
  if (!user && protectedProp) {
    return <Redirect to={`/login${encodeUri({ from: window.location.pathname + window.location.search })}`} />;
  }

  const component = (
    <div className={classNames('page', (addFooter || addHeader || addSideBar  ) && 'with_layout_page', addCategories && 'Categ'  )}>
      <BaseRoute {...props} />
    </div>
  );

  return (
    <>
    
      {addHeader && <Header />}
      {addCategories && <Categories />}
      {addSideBar && <SideBar />}
      {component}
      {addFooter && <Footer />}
    </>
  );
};

export default Route;
