import React, { useContext, useEffect, useState, forwardRef } from "react";
import {
  Navbar,
  Form,
  Button,
  FormControl,
  Nav,
  NavDropdown,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import AutoComplete from "@material-ui/lab/Autocomplete/Autocomplete";
import { Icon, InlineIcon } from "@iconify/react";
import diamondIcon from "@iconify/icons-simple-line-icons/diamond";
import Badge from "@material-ui/core/Badge";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { Link, withRouter, useHistory } from "react-router-dom";
import userContext from "../contexts/userContext";
import { setAuthorizationBearer } from "../requests/http";
import useApiState from "../hooks/useApiState";
import { getCartData } from "../requests/pannel";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import Typography from "@material-ui/core/Typography";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import Popover from "@material-ui/core/Popover";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import {
  AwesomeButton,
  AwesomeButtonProgress,
  AwesomeButtonSocial,
} from "react-awesome-button";
import Footer from "./Footer.js";
import cartContext from "../contexts/cartContext";
import Tit from "../assets/title6.png";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { updateToCart } from "../requests/pannel";

import "./scss/header.scss";
import { getProductRequest } from "../requests/product";
import TextField from "@material-ui/core/TextField/TextField";

const StyledBadge = withStyles((theme) => ({
  badge: {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}))(Badge);

const Tip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    backgroundColor: "white",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    width: "auto",
    border: "3px solid white",
    boxShadow: "3px 3px 2px 3px #888888",
    height: "auto",
    color: "black",
    fontSize: 16,
    display: "flex",
    justifyContent: "center",
  },
}))(Tooltip);
const Header = ({ children, height, protected: protectedProp, context }) => {
  const { user, setUser } = useContext(userContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const { getCartState, getCartCall } = useContext(cartContext);
  const [updateCartState, updateCartCall] = useApiState(updateToCart);
  const [getProductsState, getProductsCall] = useApiState(getProductRequest);

  const history = useHistory();
  console.log(getCartState.data);
  let sum = 0;

  getCartState.data &&
    getCartState.data.products.forEach((item, index) => {
      if (item.product?.promotion.reduction !== 0) {
        sum =
          sum +
          (item.product?.price -
            (item.product?.price * item.product?.promotion.reduction) / 100) *
            item.quantity;
      } else {
        sum = sum + item.product.price * item.quantity;
      }
    });
  useEffect(() => {
    if (updateCartState.data) getCartCall();
  }, [updateCartState.data]);
  useEffect(() => {
    if (user) {
      getCartCall();
    }
  }, [user]);
  console.log(getCartState.data?.products.length, "together");

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      backgroundColor: "white",
      boxShadow: theme.shadows[1],
      fontSize: 11,
      width: 280,
      borderRadius: 5,
      border: "3px solid white",
      boxShadow: "3px 3px 2px 3px #888888",
      marginRight: 30,
      height: "auto",
      color: "white",
      fontSize: 16,
      display: "flex",
    },
  }))(Tooltip);
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <Navbar expand="lg">
        <Link to="/">
          <Navbar.Brand href="#home" className="nav">
            <div className="title">
              <div>
                <ShoppingBasketIcon className="icon-title" />
              </div>
              <div className="text-title">cobee</div>
              <div className="text-tit">Shop</div>
            </div>
          </Navbar.Brand>
        </Link>
        <Navbar.Collapse className="searchcollapse">
          <AutoComplete
            options={getProductsState.data || []}
            getOptionLabel={(option) => option.label}
            onChange={(e, value) => {
              history.push("/produit/" + value._id);
            }}
            onInputChange={(e, value) => getProductsCall({ search: value })}
            renderInput={(props) => {
              return (
                <div className="search">
                  <TextField variant="outlined" {...props} />
                </div>
              );
            }}
          ></AutoComplete>
        </Navbar.Collapse>

        <Navbar.Collapse className="collapse">
          {user ? (
            <Tip
              title={
                <React.Fragment>
                  <div className="spend-points">
                    Vous pouvez dépenser ces points sur vos commandes, (10
                    points = 1 DT){" "}
                  </div>
                </React.Fragment>
              }
              TransitionProps={{ timeout: 700 }}
              placement="bottom"
              arrow
            >
              <div className="pointsContainer">
                <div className="myPoints">
                  {" "}
                  <div>
                    <Icon className="diamond" icon={diamondIcon} />
                  </div>
                  <div className="points">Mes points</div>
                </div>
                <div className="number">{user?.points}</div>
              </div>
            </Tip>
          ) : null}
          {!user ? (
            <Link to="/login" className="secnctMargin">
              {" "}
              <Button variant="dark" className="btnConnecter">
                SE CONNECTER
              </Button>
            </Link>
          ) : (
            <Dropdown className="account">
              <AccountCircleIcon />
              {user.firstName.length + user.lastName.length < 14 ? (
                <Dropdown.Toggle variant="" className="togglename">
                  {user.firstName} {user.lastName}
                </Dropdown.Toggle>
              ) : (
                <Dropdown.Toggle variant="" className="togglename">
                  {user.firstName.substring(0, 10)}{" "}
                  {user.lastName.substring(0, 5)}...
                </Dropdown.Toggle>
              )}
              <Dropdown.Menu>
                <Dropdown.Item href="/profile">Profile</Dropdown.Item>
                <Dropdown.Item href="/liste-favoris">
                  Listes favoris
                </Dropdown.Item>
                <Dropdown.Item
                  href="#/action-3"
                  onClick={() => {
                    localStorage.clear();
                    setAuthorizationBearer(null);
                    setUser(null);
                    handleClose();
                    if (!protectedProp) {
                      history.push("/");
                    }
                  }}
                >
                  Déconnexion
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          )}
          {user ? (
            <div
              className="cntct-us"
              onClick={() =>
                history.push({
                  pathname: "/contact-nous",
                })
              }
            >
              {" "}
              Contactez-Nous <HelpOutlineIcon className="ic-cnts" />
            </div>
          ) : null}

          <div className="icon">
            <IconButton aria-label="cart" className="shop-msg">
              <StyledBadge
                showZero={false}
                badgeContent={user ? getCartState.data?.products.length : 0}
                color="secondary"
              >
                <LightTooltip
                  interactive={true}
                  title={
                    <React.Fragment>
                      {!getCartState.data?.products.length ? (
                        <div className="shp-not">
                          <p className="shp-vide">Votre panier est vide</p>
                        </div>
                      ) : (
                        <div className="shp-hov">
                          {getCartState.data &&
                            getCartState.data?.products.map((item, index) => (
                              <div key={item._id}>
                                <div className="hov-container">
                                  <div className="img-tac">
                                    <img
                                      className="img-hove"
                                      src={item.product?.images}
                                    />
                                  </div>
                                  <div>
                                    {item.product?.label.length <= 21 ? (
                                      <p className="shp-hov">
                                        {item.product?.label}{" "}
                                      </p>
                                    ) : (
                                      <p className="shp-hov">
                                        {item.product?.label.substring(0, 20)}
                                        ...{" "}
                                      </p>
                                    )}
                                    <div className="ihg">
                                      {item.product?.promotion.reduction !==
                                      0 ? (
                                        <div>
                                          <p className="shp-prix">
                                            {(item.product?.price -
                                              (item.product?.price *
                                                item.product?.promotion
                                                  .reduction) /
                                                100) *
                                              item.quantity}{" "}
                                            TND
                                          </p>
                                        </div>
                                      ) : (
                                        <p className="shp-prix">
                                          {item.product?.price * item.quantity}{" "}
                                          TND
                                        </p>
                                      )}
                                      <p className="ihet">x{item.quantity}</p>
                                    </div>
                                  </div>
                                  <div>
                                    <DeleteForeverIcon
                                      onClick={() => {
                                        updateCartCall({
                                          products: getCartState.data.products.filter(
                                            (item, i) => index !== i
                                          ),
                                        });
                                      }}
                                      className="del-hov"
                                    />
                                  </div>
                                </div>
                                <div></div>
                                <div className="ligne-hov"></div>
                              </div>
                            ))}{" "}
                          <div className="btnck-container">
                            <div className="tot-cont">
                              <p className="tot-ck">Total: </p>
                            </div>

                            <div className="sum-ck">
                              <p>{sum} TND</p>
                            </div>
                            <AwesomeButton
                              type="pinterest  hover pressure"
                              className="btn-ck"
                            >
                              Check-Out
                            </AwesomeButton>
                          </div>
                        </div>
                      )}
                    </React.Fragment>
                  }
                  arrow
                  TransitionProps={{ timeout: 100 }}
                  placement="top"
                >
                  <ShoppingCartIcon
                    onClick={() => history.push("/panier")}
                    className="shopIcon"
                  />
                </LightTooltip>
              </StyledBadge>
            </IconButton>
          </div>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};
export default withRouter(Header);
