import React, { useContext, useState } from "react";
import {
  Button,
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";
import Grid from "@material-ui/core/Grid";

import { Link, Route, Switch, useHistory } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Img from "../assets/pc.png";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PhoneIcon from "@material-ui/icons/Phone";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import HelpIcon from "@material-ui/icons/Help";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";
import ThumbDown from "@material-ui/icons/ThumbDown";
import ThumbUp from "@material-ui/icons/ThumbUp";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Fade";

import "./scss/Categories.scss";

const Category = ({ match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [selectedDate, setSelectedDate] = React.useState(
    new Date("2014-08-18T21:11:54")
  );
  const history = useHistory();
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const { user } = useContext(userContext);

  console.log(categoryState);

  useDidMount(() => {
    categoryCall();
    subCall();
  });

  console.log(subState, "toto");

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      "aria-controls": `scrollable-force-tabpanel-${index}`,
    };
  }

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
  }));
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      backgroundColor: "#607d8b",
      boxShadow: theme.shadows[1],
      fontSize: 11,
      "&:hover": {
        background: "#607d8b",
      },
      width: 200,
      height: "auto",
      color: "white",
      fontSize: 17,
      display: "flex",
      justifyContent: "center",
    },
  }))(Tooltip);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div>
      {" "}
      <AppBar position="static" className="marg-container" color="default">
        <Tabs
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          aria-label="scrollable force tabs example"
        >
          {categoryState.data &&
            categoryState.data.map((item, index) => (
              <div >
                <LightTooltip
                key={item._id}
                  onClick={() =>
                    history.push({
                      pathname: `/categ/${item._id}`,
                    })
                  }
                  interactive={true}
                  title={
                    <div className="work-tip">
                      {subState.data &&
                        subState.data
                          .filter((sub) => {
                            return sub.category === item._id;
                          })
                          .map((sub, index) => (
                            <div className="bord-let" onClick={()=> history.push({
                pathname: `/subcategory/${sub._id}`,
              })}>
                              <Grid container spacing={3} className="mr-resist">
                                <Grid item xs={3}>
                                  <img className="ima-test" src={sub.icon} />
                                </Grid>
                                <Grid item xs={6} className="cm-wow">
                                  <p className="ima-text">{sub.label} </p>
                                </Grid>
                              </Grid>
                            </div>
                          ))}{" "}
                    </div>
                  }
                  TransitionProps={{ timeout: 700 }}
                  TransitionComponent={Zoom}
                  placement="bottom"
                  arrow
                >
                  <Tab
                    className="autre-test"
                    label={item.label}
                    icon={<img src={item.icon} className="testet" />}
                  />
                </LightTooltip>
              </div>
            ))}
        </Tabs>
      </AppBar>
      <div className="btnCatego">
        <Link to="/Categories">
          <Button className="btnCat" variant="outline-dark">
            Afficher Tout
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Category;
