import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import RootContainer from './containers/RootContainer';
import AdminrootContainer from './adminContainers/AdminRootContainer';
import 'react-toastify/dist/ReactToastify.css';
import 'rodal/lib/rodal.css';


import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "react-awesome-button/dist/styles.css";

const App = () => (
  <BrowserRouter>
    <RootContainer />
  </BrowserRouter>
);

export default App;
