import React, { useContext, useState } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";

import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Img from "../assets/pc.png";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import "./scss/profilecontainer.scss";
import "./scss/ordercontainer.scss";

const ProfileContainer = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [hex, setHex] = useState("#ffffff");

  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  const { user } = useContext(userContext);

  const uri = match.params;

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    subCall();
    randomizeHex();
  });
  console.log(user);
  return (
    <div>
      {" "}
      <div className="prof-left">
        <div className="circle-text" style={{ backgroundColor: `${hex}` }}>
          <p className="inside-circle">{user.firstName.substring(0, 1)}</p>
        </div>
        <div>
          <div className="fi-name">
            {" "}
            Bonjour, {user.firstName} {user.lastName}
          </div>
          <div className="la-name"> {user.email}</div>
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Paper className="pap-leftuser">
            <div
              className="wuk"
              onClick={() =>
                history.push({
                  pathname: `/mesorders`,
                })
              }
            >
              {" "}
              <HistoryIcon className="wuk-one" />
              <div className="wuk-two">Historiques des commandes</div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <AccountCircleIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Profile
              </div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <LoyaltyIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/liste-favoris`,
                  })
                }
              >
                Listes des favoris
              </div>
            </div>
          </Paper>{" "}
        </Grid>
        <Grid item xs={8} className="pap-right">
          <Paper className="user-right">
            <div className="info-prof">Profile Information</div>{" "}
            <div className="info-all">Nom : {user.firstName} </div>{" "}
            <div className="info-all">Prenom : {user.lastName} </div>{" "}
            {user && !user.facebookId && (
              <div className="info-all">Email : {user.email} </div>
            )}
            <div className="twobtns">
              <Button
                variant="contained"
                className="btn-profile"
                onClick={() =>
                  history.push({
                    pathname: `/edituser`,
                  })
                }
              >
                Modifer
              </Button>{" "}
              {user && !user.facebookId && (
                <Button
                  variant="co   ntained"
                  className="btn-passw"
                  onClick={() =>
                    history.push({
                      pathname: `/changepass`,
                    })
                  }
                >
                  Changer mot de pass
                </Button>
              )}
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default ProfileContainer;
