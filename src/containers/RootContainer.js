import React, { useState } from "react";
import { Route as BaseRoute, Switch, useHistory } from "react-router-dom";
import UserContext from "../contexts/userContext";
import { FacebookProvider } from "react-facebook";
import { ToastContainer } from "react-toastify";

// hoc
import Route from "../layout/Route";
// containers
import HomeContainer from "./HomeContainer";
import LoginContainer from "./LoginContainer";
import CategoriesContainer from "./CategoriesContainer";
import RegisterContainer from "./RegisterContainer";
import ProductContainer from "./ProductContainer";
import CategContainer from "./categoryContainer";
import PannelContainer from "./pannelContainer";
import checkoutContainer from "./checkoutContainer";
import SubCategContainer from "./subcategoryContainer";
import OrdersContainer from "./ordersContainer";
import ProfileContainer from "./profileContainer";
import ChangePass from "./changepassword";
import Contact from "./contact";
import FavoriteList from "./favoriteList";

import Edituser from "./editUser";

import Test from "./tests";
import { getCartData } from "../requests/pannel";
import { getReview } from "../requests/product";

import useApiState from "../hooks/useApiState";
import CartContext from "../contexts/cartContext";
import AdminRootContainer from "../adminContainers/AdminRootContainer";
import CommentContext from "../contexts/commentContext";

// styles
import classNames from "../utils/classNames";
// hooks

import { useDidMount } from "../hooks/useLifeCycle";
// requests
import { refreshRequest } from "../requests/auth";
import "./scss/root.scss";
import { setAuthorizationBearer } from "../requests/http";
const RootContainer = () => {
  const [startup, setStartup] = useState(false);
  const [user, setUser] = useState(null);
  const history = useHistory();
  const [getCartState, getCartCall] = useApiState(getCartData);
  const [getReviewsState, getReviewsCall] = useApiState(getReview);

  async function onStart() {
    try {
      const t = localStorage.getItem("token");
      if (t) {
        const data = JSON.parse(t);
        const tokenResponse = await refreshRequest({
          email: data.user.email,
          facebookId: data.user.facebookId,
          refreshToken: data.token.refreshToken,
        });
        if (tokenResponse.status === "OK") {
          setAuthorizationBearer(tokenResponse.data.accessToken);
          localStorage.setItem(
            "token",
            JSON.stringify({ user: data.user, token: tokenResponse.data })
          );
          setUser(data.user);
        }
      }
      setStartup(true);
    } catch (e) {
      setStartup(true);
    }
  }
  useDidMount(() => {
    onStart().then((nextUser) => {
      if (nextUser) setUser(nextUser);
      setStartup(true);
    });

    history.listen(() => {
      window.scrollTo({ top: 0, left: 0 });
    });
  });
  if (!startup) return <div />;
  return (
    <FacebookProvider appId="780086846117318">
      <UserContext.Provider value={{ user, setUser }}>
        <CommentContext.Provider value={{ getReviewsState, getReviewsCall }}>
          <CartContext.Provider value={{ getCartState, getCartCall }}>
            <div
              className={classNames("containers_root", "width_100 padding_20")}
            >
              <Switch>
                <Route
                  exact
                  path="/"
                  addHeader
                  addCategories
                  component={HomeContainer}
                />
                <Route
                  exact
                  path="/login"
                  addCategories
                  addHeader
                  component={LoginContainer}
                />
                <Route
                  exact
                  path="/Categories"
                  addCategories
                  addHeader
                  component={CategoriesContainer}
                />
                <Route
                  exact
                  path="/register"
                  addCategories
                  addHeader
                  component={RegisterContainer}
                />
                <Route
                  exact
                  path="/categ/:id"
                  addCategories
                  addHeader
                  component={CategContainer}
                />
                <Route
                  exact
                  path="/liste-favoris"
                  addCategories
                  addHeader
                  component={FavoriteList}
                />
                <Route
                  exact
                  path="/contact-nous"
                  addHeader
                  component={Contact}
                />
                <Route
                  exact
                  path="/panier"
                  addHeader
                  component={PannelContainer}
                />
                <Route
                  exact
                  path="/checkout"
                  addHeader
                  component={checkoutContainer}
                />
                <Route exact path="/test" addHeader component={Test} />

                <Route
                  exact
                  path="/subcategory/:id"
                  addCategories
                  addHeader
                  component={SubCategContainer}
                />

                <Route
                  exact
                  path="/produit/:id"
                  addCategories
                  addHeader
                  component={ProductContainer}
                />
                <Route
                  exact
                  path="/mesorders"
                  addCategories
                  addHeader
                  component={OrdersContainer}
                />
                <Route
                  protected
                  exact
                  path="/profile"
                  addCategories
                  addHeader
                  component={ProfileContainer}
                />
                <Route
                  exact
                  path="/changepass"
                  addCategories
                  addHeader
                  component={ChangePass}
                />
                <Route
                  exact
                  path="/edituser"
                  addCategories
                  addHeader
                  component={Edituser}
                />
                <BaseRoute path="/admin" component={AdminRootContainer} />
              </Switch>
            </div>
            <ToastContainer></ToastContainer>
          </CartContext.Provider>
        </CommentContext.Provider>
      </UserContext.Provider>
    </FacebookProvider>
  );
};
export default RootContainer;
