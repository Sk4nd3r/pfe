import React, { useContext, useState } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";

import Img from "../assets/pc.png";

import "./scss/categContainer.scss";

const CategContainer = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall]=useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);

  const { user } = useContext(userContext);
  
  const uri = match.params;
  console.log(uri);
  console.log(categoryState);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    categCall(uri);
    subCall();
  });
  console.log(uri,"toto");

  console.log(subState,"toto");
  const card = [
    {
      test: "",
    },
    {
      test: "",
    },
    {
      test: "",
    },
    {
      test: "",
    },
    {
      test: "",
    },
    {
      test: "",
    },
    {
      test: "",
    },
  ];
  return (
    <div>
      <div className="cat-container">
        <center className="titlecateg">{categState.data?.label}</center>
        <Slider {...settings}>
          {subState.data &&
                    subState.data
                      .filter((sub) => sub.category === uri.id)
                      .map((sub) => (
            <div className="car-over" key={sub._id}>
              <Card className="img-over" style={{ width: "16rem" }}>
                <Card.Img className="car-img" variant="top" src={sub.image} />
                <Card.Body>
                  <center className="subcatLabel">{sub.label}</center>
                  <Button variant="contained" color="secondary" className="achterbtn"   onClick={() =>
              history.push({
                pathname: `/subcategory/${sub._id}`,
              })} >
                    Acheter Maintenant
                  </Button>
                </Card.Body>
              </Card>
            </div>
          ))}
        </Slider>
      </div>

      <div className="populaire-container">
        <p className="populaire-title">Produits les plus populaires</p>
        <Slider {...settings}>
          {card.map((item, id) => (
            <div className="car-over">
              <Card key={id} className="img-over" style={{ width: "16rem" }}>
                <Card.Img className="car-img" variant="top" src={Img} />
                <Card.Body>
                  <center className="subcatLabel">subCategory Title</center>
                  <Button variant="contained" color="secondary" className="achterbtn">
                    Acheter Maintenant
                  </Button>
                </Card.Body>
              </Card>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default CategContainer;
