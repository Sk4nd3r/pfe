import React, { useContext, useEffect, useState } from "react";
import userContext from "../contexts/userContext";
import { RouteComponentProps, Redirect, Link } from "react-router-dom";
import { registerRequest } from "../requests/auth";
import { decodeUri } from "../utils/url";
import { Alert } from "react-bootstrap";
import useApiState from "../hooks/useApiState";
import { useForm } from "../hooks/useInputs";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { useDidUpdate } from "../hooks/useLifeCycle";
import { getProductRequest } from "../requests/product";
import ErrorIcon from "@material-ui/icons/Error";
import {
  validateEmail,
  validatePassword,
  isStringEmpty,
} from "../utils/validation";

import {
  Form,
  Button,
  FormControl,
  Nav,
  NavDropdown,
  Col,
  Container,
  Row,
} from "react-bootstrap";
import { toast, zoomn, bounce } from "react-toastify";

import "./scss/form.scss";
import { setAuthorizationBearer } from "../requests/http";

const RegisterContainer = ({ location }) => {
  const { user, setUser } = useContext(userContext);
  const [registerState, register] = useApiState(registerRequest);
  const [formState, formActions] = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      confirmPassword: "",
    },
    validation: {
      firstName: isStringEmpty,
      lastName: isStringEmpty,
      email: validateEmail,
      password: validatePassword,
      confirmPassword: validatePassword,
    },
  });
  const successToast = () => {
    toast("inscription effectué avec succès", {
      className: "custom-toast",
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
    });
  };
  useDidUpdate(() => {
    if (!registerState.fetching) {
      if (
        !registerState.errors &&
        !registerState.errorCode &&
        registerState.data
      ) {
        localStorage.setItem("token", JSON.stringify(registerState.data));
        setAuthorizationBearer(registerState.data.token.accessToken);
        setUser(registerState.data.user);
      } else if (registerState.errorCode === "EMAIL_ALREADY_EXIST") {
        formActions.setErrors({
          email: "Email already exist",
        });
      } else if (registerState.errorCode) {
        alert("error");
      }
    }
  }, [registerState.fetching]);

  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  console.log({ register, registerState });

  if (user) {
    console.log(user);
    return <Redirect to={from || "/"} />;
  }
  function onSubmit(e) {
    e.preventDefault();
    const { email, password, firstName, lastName } = formState.values;

    if (formActions.validateForm()) {
      if (values.password === values.confirmPassword) {
        register({
          email,
          password,
          firstName,
          lastName,
        });
        successToast();
      } else {
        formActions.setErrors({
          confirmPassword: "Les mots de passe ne correspondent pas",
        });
      }
    } else {
      formActions.setAllTouched(true);
    }
    console.log(errors);
    console.log(formState);
  }

  return (
    <div className="home width_100">
      <div className="formContainer">
        <h2 onClick={successToast}>Créer votre compte sur CobeeShop</h2>
        <Form onSubmit={onSubmit} className="form">
          <Form.Group className="formNom">
            <Row>
              <Col>
                {" "}
                <Form.Label>Nom </Form.Label>
                <Form.Control
                  className={
                    touched.firstName && errors.firstName.length > 0
                      ? "error"
                      : null
                  }
                  type="nom"
                  onChange={handleChange}
                  name="firstName"
                  placeholder="Nom"
                  value={values.firstName}
                />
                {touched.firstName && errors.firstName && (
                  <div>
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">{errors.firstName}</span>
                  </div>
                )}
              </Col>
              <div className="eye2">
                {" "}
                <VisibilityOffIcon onClick={togglePasswordVisiblity} />
              </div>

              <Col>
                {" "}
                <Form.Label>Prénom </Form.Label>
                <Form.Control
                  className={
                    touched.lastName && errors.lastName.length > 0
                      ? "error"
                      : null
                  }
                  type="prenom"
                  onChange={handleChange}
                  name="lastName"
                  value={values.lastName}
                  placeholder="Prénom"
                />
                {touched.lastName && errors.lastName && (
                  <div>
                    {" "}
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">{errors.lastName}</span>
                  </div>
                )}
              </Col>
            </Row>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Addresse Email </Form.Label>
            <Form.Control
              className={
                touched.email && errors.email.length > 0 ? "error" : null
              }
              type="email"
              onChange={handleChange}
              name="email"
              value={values.email}
              placeholder="Email"
            />
            {touched.email && errors.email && (
              <div>
                <ErrorIcon className="errorIcon" fontSize="small" />
                <span className="errorMessage">{errors.email}</span>
              </div>
            )}
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Row>
              <Col className="showpass2">
                {" "}
                <Form.Label>Mot de passe</Form.Label>
                <div>
                  <Form.Control
                    className={
                      touched.password && errors.password.length > 0
                        ? "error"
                        : "trypass"
                    }
                    type={passwordShown ? "text" : "password"}
                    onChange={handleChange}
                    name="password"
                    value={values.password}
                    placeholder="Mot de pass"
                  />
                  {touched.password && errors.password && (
                    <div>
                      {" "}
                      <ErrorIcon className="errorIcon" fontSize="small" />
                      <span className="errorMessage">{errors.password}</span>
                    </div>
                  )}
                </div>
              </Col>
              <div className={passwordShown ? "eyeNotshown" : "eyeShown"}>
                {" "}
                <VisibilityOffIcon onClick={togglePasswordVisiblity} />{" "}
              </div>
              <div className={passwordShown ? "eyeShown" : "eyeNotshown"}>
                {" "}
                <VisibilityIcon onClick={togglePasswordVisiblity} />{" "}
              </div>

              <Col className="showpass">
                {" "}
                <Form.Label>Confirmer Mot de passe</Form.Label>
                <Form.Control
                  className={
                    touched.confirmPassword && errors.confirmPassword.length > 0
                      ? "error"
                      : null
                  }
                  type={passwordShown ? "text" : "password"}
                  onChange={handleChange}
                  name="confirmPassword"
                  value={values.confirmPassword}
                  placeholder="Confirmer"
                />
                {touched.confirmPassword && errors.confirmPassword && (
                  <div>
                    {" "}
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">
                      {errors.confirmPassword}
                    </span>
                  </div>
                )}
              </Col>
            </Row>
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default RegisterContainer;
