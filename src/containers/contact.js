import React, { useContext, useState } from "react";
import { Navbar, Form, Nav, Container, Row, Col } from "react-bootstrap";
import userContext from "../contexts/userContext";
import Select from "@material-ui/core/Select";
import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Img from "../assets/pc.png";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import TextField from "@material-ui/core/TextField";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextareaAutosizes from "@material-ui/core/TextareaAutosize";
import EmailIcon from "@material-ui/icons/Email";
import "./scss/contact.scss";
import "./scss/ordercontainer.scss";
import { useForm } from "../hooks/useInputs";
import { createContact } from "../requests/assistance";
import { toast, zoomn, bounce } from "react-toastify";

const Contact = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [createContactState, createContactCall] = useApiState(createContact);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [hex, setHex] = useState("#ffffff");
  const [{ values }, { handleChange }] = useForm({
    initialValues: {
      type: "",
      text: "",
      phone:"",
    },
  });
  const contactToast = () => {
    toast.info("votre ticket a été envoyé à l'administration", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  const { user } = useContext(userContext);

  const uri = match.params;
  console.log(uri);
  console.log(categoryState);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    categCall(uri);
    subCall();
    randomizeHex();
  });
  console.log(user, "te");

  console.log(subState, "toto");

  return (
    <div>
      <div className="cont-shop">
        <ExitToAppIcon />{" "}
        <p className="continue-shop" onClick={() => history.push("/")}>
          Continuer vos achats
        </p>
      </div>
      <div className="ligne-horz"></div>
      <div className="cntr-ctct">
        <div className="contact-container">
          <div className="dis-tr">
            <p className="ctct-nou">
              {" "}
              <EmailIcon className="em-edi" />
              Contactez-nous
            </p>
          </div>
          <div className="cntr-ctct">
            <FormControl className="direction-inputs">
              <div>
                <TextField
                  className="tel_ph"
                  label="Numéro de téléphone"
                  variant="standard"
                  name="phone"
                  value={values.phone}

                  onChange={handleChange}
                />
              </div>
              <div className="res-tyf">
                <Select
                  className="resi-sgl"
                  value={values.type}
                  onChange={handleChange}
                  name="type"
                >
                  <MenuItem value={"Assistance technique"}>
                    Assistance technique
                  </MenuItem>
                  <MenuItem value={"Assistance commercial"}>
                    Assistance commercial
                  </MenuItem>
                  <MenuItem value={"Autre"}>Autre</MenuItem>
                </Select>
                <FormHelperText className="res-tyf">Choisir une raison</FormHelperText>{" "}
              </div>
            </FormControl>
          </div>
          <div className="cntr-ctct">
            {" "}
            <TextareaAutosizes
              className="sig-ctct"
              rowsMin={4}
              rowsMax={4}
              type="text"
              name="text"
              placeholder="Ecrire votre message"
              value={values.text}
              onChange={handleChange}
            />
          </div>
          <div className="cntr-ctct">
            <Button
              variant="contained"
              color="secondary"
              className="btn-ctctnous"
              onClick={() => {
                createContact(values);
                contactToast();
                history.push("/")
              }}
            >
              Envoyer
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
