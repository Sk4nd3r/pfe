import React, { useContext, useEffect, useState } from "react";
import userContext from "../contexts/userContext";
import Navbar from "../layout/Header";
import Footer from "../layout/Footer";
import { Card, Carousel, Dropdown } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import {
  getProductRequest,
  getMostPopular,
  getRecommanded,
} from "../requests/product";

import { addToCart } from "../requests/pannel";
import { getReview } from "../requests/product";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { toast, zoomn, bounce } from "react-toastify";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import { useDidMount } from "../hooks/useLifeCycle";
import Button from "@material-ui/core/Button";
import cartContext from "../contexts/cartContext";
import Img from "../assets/pc.png";
import {
  addToFavorite,
  getFavorite,
  updateFavorite,
} from "../requests/wishlist";

import "./scss/home.scss";

const HomeContainer = ({ history }) => {
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  const [value, setValue] = React.useState(4.5);
  const [cartState, cartCall] = useApiState(addToCart);
  const [favoriteState, favoriteCall] = useApiState(addToFavorite);
  const [getFavoriteState, getFavoriteCall] = useApiState(getFavorite);
  const [updateFavoriteState, updateFavoriteCall] = useApiState(updateFavorite);

  const [mostPopState, mostPopCall] = useApiState(getMostPopular);
  const [getReviewsState, getReviewsCall] = useApiState(getReview);
  const [productState, productCall] = useApiState(getProductRequest);
  const [recommandedState, recommandedCall] = useApiState(getRecommanded);
  const [view, setView] = useState(false);

  const { getCartState, getCartCall } = useContext(cartContext);
  const infotoast = () => {
    toast.info("vous avez ajouter un produit au panier", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };

  const hearttoast = () => {
    toast.info("vous avez ajouter un produit au liste des favoris", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const removeheartToast = () => {
    toast.info("vous avez retier un produit du la liste des favoris", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  useDidMount(() => {
    productCall(/* {promotion:true} */);
    mostPopCall();
    getReviewsCall();
    recommandedCall();
    getFavoriteCall();
  });
  useEffect(() => {
    if (!cartState.fetching && cartState.data) {
      getCartCall();
    }
  }, [cartState.fetching]);

  useEffect(() => {
    if (!favoriteState.fetching && favoriteState.data) {
      getFavoriteCall();
    }
  }, [favoriteState.fetching]);
  useEffect(() => {
    if (updateFavoriteState.data) getFavoriteCall();
  }, [updateFavoriteState.data]);
  console.log(productState.data, "ererererere");

  console.log(getFavoriteState, "orororororor");
  const [hover, setHover] = React.useState(-1);
  const labels = {
    0.5: "Useless",
    1: "Useless+",
    1.5: "Poor",
    2: "Poor+",
    2.5: "Ok",
    3: "Ok+",
    3.5: "Good",
    4: "Good+",
    4.5: "Excellent",
    5: "Excellent+",
  };

  return (
    <div>
      <div className="promoTitle">
        <h5>Les produits avec promotions</h5>
        <div className="slide-container">
          <Slider {...settings}>
            {productState.data &&
              productState.data.filter((row) => row.promotion.reduction !== 0).map((item, index) => (
                <div className="cards" key={item._id}>
                  <Card className="imgContainer" style={{ width: "15rem" }}>
                    <div>
                      {item.promotion.reduction !== 0 ? (
                        <div className="triangle-img">
                          <div className="triangle-content">
                            -{item.promotion.reduction}%
                          </div>
                        </div>
                      ) : (
                        <div></div>
                      )}

                      <div
                        className="heart-prod"
                        onClick={() => {
                          if (
                            getFavoriteState.data &&
                            !getFavoriteState.data.products.find(
                              (row) => row.product?._id === item._id
                            )
                          ) {
                            favoriteCall({
                              products: [{ product: item._id }],
                            });
                          } else {
                            updateFavoriteCall({
                              products: getFavoriteState.data?.products.filter(
                                (row, i) => row.product._id !== item._id
                              ),
                            });
                          }
                        }}
                      >
                        <FavoriteIcon
                          className={
                            getFavoriteState.data &&
                            getFavoriteState.data.products.find(
                              (row) => row.product?._id === item._id
                            )
                              ? "col-heart-active"
                              : "col-heart"
                          }
                        />
                      </div>

                      <Card.Img
                        className="cardImage"
                        variant="top"
                        src={item.images[0]}
                        onClick={() =>
                          history.push({
                            pathname: `/produit/${item._id}`,
                          })
                        }
                      />
                    </div>
                    {item.label.length > 30 ? (
                      <center className="label-prod">
                        {item.label.substring(0, 27)}...
                      </center>
                    ) : (
                      <center className="label-prod">{item.label}</center>
                    )}

                    <Card.Body className="noPadding">
                      <div className="descriptionContainer">
                        {item.description.length > 50 ? (
                          <Card.Text className="descriptiontext">
                            {" "}
                            {item.description.substring(0, 49)}...{" "}
                          </Card.Text>
                        ) : (
                          <Card.Text className="descriptionother">
                            {" "}
                            {item.description} .
                          </Card.Text>
                        )}
                      </div>
                      <Box
                        component="fieldset"
                        mb={3}
                        borderColor="transparent"
                        className="fieldsetPadding"
                      >
                        <div className="stars">
                          <Rating
                            disabled
                            name="half-rating-read"
                            value={item.rating}
                            precision={0.5}
                            readOnly
                            /*
                          onChange={(event, newValuee) => {
                            setValuee(newValuee);
                          }} */
                            /* onChangeActive={(event, newHover) => {
                            setHover(newHover);
                          }} */
                            className="ratingStars"
                          />{" "}
                          {/*                               {value !== null && <Box ml={2}>{labels[hover !== -1 ? hover : valuee]}</Box>}
                           */}
                          <div className="avisContainer">
                            <b className="avisPts">{item.rating} </b>{" "}
                            <b className="avis"> /5</b>
                          </div>
                        </div>
                        <div className="ligne"></div>
                        <div className="pricePoints">
                          {item.promotion.reduction !== 0 ? (
                            <div>
                              <div className="priceLabel">
                                {item.price -
                                  (item.price * item.promotion.reduction) /
                                    100}{" "}
                                TND
                              </div>{" "}
                              <div className="priceLabel-promo">
                                {item.price} TND
                              </div>
                            </div>
                          ) : (
                            <div className="priceLabel">{item.price} TND</div>
                          )}

                          <Card.Text className="pointsLabel">
                            {item.credit} points
                          </Card.Text>
                        </div>
                      </Box>
                      <Button
                        variant="outlined"
                        color="secondary"
                        className={
                          getCartState.data &&
                          getCartState.data?.products.find(
                            (row) => row.product?._id !== item._id
                          )
                            ? "addcart"
                            : "addarts"
                        }
                        onClick={() => {
                          cartCall({
                            products: [{ product: item._id, quantity: 1 }],
                          });
                          infotoast();
                        }}
                        disabled={
                          getCartState.data &&
                          getCartState.data?.products.find(
                            (row) => row.product?._id === item._id
                          )
                            ? true
                            : false
                        }
                      >
                        {getCartState.data &&
                        getCartState.data?.products.find(
                          (row) => row.product?._id === item._id
                        ) ? (
                          <b className="addcartLabel">Déjà ajouté au panier</b>
                        ) : (
                          <b className="addcartLabel">Ajouter au panier</b>
                        )}
                      </Button>{" "}
                    </Card.Body>
                  </Card>
                </div>
              ))}
          </Slider>
        </div>
      </div>

      <div className="promoTitle">
        <h5>Les porduits les plus populaires</h5>
        <div className="slide-container">
          <Slider {...settings}>
            {mostPopState.data &&
              mostPopState.data.map(
                (item, index) =>
                  item.product && (
                    <div className="cards" key={item._id}>
                      <Card className="imgContainer" style={{ width: "15rem" }}>
                        <div>
                          {item.product.promotion.reduction !== 0 ? (
                            <div className="triangle-img">
                              <div className="triangle-content">
                                -{item.product.promotion.reduction}%
                              </div>
                            </div>
                          ) : (
                            <div></div>
                          )}
                          <div
                            className="heart-prod"
                            onClick={() => {
                              if (
                                getFavoriteState.data &&
                                !getFavoriteState.data.products.find(
                                  (row) => row.product?._id === item.product._id
                                )
                              ) {
                                favoriteCall({
                                  products: [{ product: item.product._id }],
                                });
                              } else {
                                updateFavoriteCall({
                                  products: getFavoriteState.data?.products.filter(
                                    (row, i) =>
                                      row.product._id !== item.product._id
                                  ),
                                });
                              }
                            }}
                          >
                            <FavoriteIcon
                              className={
                                getFavoriteState.data &&
                                getFavoriteState.data.products.find(
                                  (row) => row.product?._id === item.product._id
                                )
                                  ? "col-heart-active"
                                  : "col-heart"
                              }
                            />{" "}
                          </div>
                          <Card.Img
                            className="cardImage"
                            variant="top"
                            src={item.product.images[0]}
                            onClick={() =>
                              history.push({
                                pathname: `/produit/${item.product._id}`,
                              })
                            }
                          />
                        </div>
                        {item.product.label.length > 30 ? (
                          <center className="label-prod">
                            {item.product.label.substring(0, 27)}...
                          </center>
                        ) : (
                          <center className="label-prod">
                            {item.product.label}
                          </center>
                        )}

                        <Card.Body className="noPadding">
                          <div className="descriptionContainer">
                            {item.product.description.length > 50 ? (
                              <Card.Text className="descriptiontext">
                                {" "}
                                {item.product.description.substring(
                                  0,
                                  49
                                )}...{" "}
                              </Card.Text>
                            ) : (
                              <Card.Text className="descriptionother">
                                {" "}
                                {item.product.description} .
                              </Card.Text>
                            )}
                          </div>
                          <Box
                            component="fieldset"
                            mb={3}
                            borderColor="transparent"
                            className="fieldsetPadding"
                          >
                            <div className="stars">
                              <Rating
                                disabled
                                name="half-rating-read"
                                value={value}
                                precision={0.5}
                                readOnly
                                className="ratingStars"
                              />{" "}
                              <div className="avisContainer">
                                <b className="avisPts">
                                  {item.product.rating}{" "}
                                </b>{" "}
                                <b className="avis"> /5</b>
                              </div>
                            </div>
                            <div className="ligne"></div>
                            <div className="pricePoints">
                              {item.product.promotion.reduction !== 0 ? (
                                <div>
                                  <div className="priceLabel">
                                    {item.product.price -
                                      (item.product.price *
                                        item.product.promotion.reduction) /
                                        100}{" "}
                                    TND
                                  </div>{" "}
                                  <div className="priceLabel-promo">
                                    {item.product.price} TND
                                  </div>
                                </div>
                              ) : (
                                <div className="priceLabel">
                                  {item.product.price} TND
                                </div>
                              )}

                              <Card.Text className="pointsLabel">
                                {item.product.credit} points
                              </Card.Text>
                            </div>
                          </Box>
                          <Button
                            variant="outlined"
                            color="secondary"
                            className="addcart"
                            onClick={() => {
                              cartCall({
                                products: [{ product: item._id, quantity: 1 }],
                              });
                              infotoast();
                            }}
                            disabled={
                              getCartState.data &&
                              getCartState.data?.products.find(
                                (row) => row.product?._id === item.product._id
                              )
                                ? true
                                : false
                            }
                          >
                            {getCartState.data &&
                            getCartState.data?.products.find(
                              (row) => row.product?._id === item.product._id
                            ) ? (
                              <b className="addcartLabel">
                                Déjà ajouté au panier
                              </b>
                            ) : (
                              <b className="addcartLabel">Ajouter au panier</b>
                            )}{" "}
                          </Button>{" "}
                        </Card.Body>
                      </Card>
                    </div>
                  )
              )}
          </Slider>
        </div>
      </div>
      <div className="promoTitle">
        <h5>Les produits recommendés pour vous</h5>
        <div className="slide-container">
          <Slider {...settings}>
            {recommandedState.data &&
              recommandedState.data.map((item, index) => (
                <div className="cards" key={item._id}>
                  <Card className="imgContainer" style={{ width: "15rem" }}>
                    <div>
                      {item.promotion && item.promotion.reduction !== 0 ? (
                        <div className="triangle-img">
                          <div className="triangle-content">
                            -{item.promotion.reduction}%
                          </div>
                        </div>
                      ) : (
                        <div></div>
                      )}
                      <div
                        className="heart-prod"
                        onClick={() => {
                          favoriteCall({
                            products: [{ product: item._id }],
                          });
                          hearttoast();
                        }}
                      >
                        <FavoriteIcon
                          className={
                            getFavoriteState.data &&
                            getFavoriteState.data.products.find(
                              (row) => row.product?._id === item._id
                            )
                              ? "col-heart-active"
                              : "col-heart"
                          }
                        />{" "}
                      </div>
                      <Card.Img
                        className="cardImage"
                        variant="top"
                        src={item.images[0]}
                        onClick={() =>
                          history.push({
                            pathname: `/produit/${item._id}`,
                          })
                        }
                      />
                    </div>
                    {item.label.length > 30 ? (
                      <center className="label-prod">
                        {item.label.substring(0, 27)}...
                      </center>
                    ) : (
                      <center className="label-prod">{item.label}</center>
                    )}

                    <Card.Body className="noPadding">
                      <div className="descriptionContainer">
                        {item.description.length > 50 ? (
                          <Card.Text className="descriptiontext">
                            {" "}
                            {item.description.substring(0, 49)}...{" "}
                          </Card.Text>
                        ) : (
                          <Card.Text className="descriptionother">
                            {" "}
                            {item.description} .
                          </Card.Text>
                        )}
                      </div>
                      <Box
                        component="fieldset"
                        mb={3}
                        borderColor="transparent"
                        className="fieldsetPadding"
                      >
                        <div className="stars">
                          <Rating
                            disabled
                            name="half-rating-read"
                            value={item.rating}
                            precision={0.5}
                            readOnly
                            /*
                          onChange={(event, newValuee) => {
                            setValuee(newValuee);
                          }} */
                            /* onChangeActive={(event, newHover) => {
                            setHover(newHover);
                          }} */
                            className="ratingStars"
                          />{" "}
                          {/*                               {value !== null && <Box ml={2}>{labels[hover !== -1 ? hover : valuee]}</Box>}
                           */}
                          <div className="avisContainer">
                            <b className="avisPts">{item.rating} </b>{" "}
                            <p className="avis"> /5</p>
                          </div>
                        </div>
                        <div className="ligne"></div>
                        <div className="pricePoints">
                          {item.promotion.reduction !== 0 ? (
                            <div>
                              <div className="priceLabel">
                                {item.price -
                                  (item.price * item.promotion.reduction) /
                                    100}{" "}
                                TND
                              </div>{" "}
                              <div className="priceLabel-promo">
                                {item.price} TND
                              </div>
                            </div>
                          ) : (
                            <div className="priceLabel">{item.price} TND</div>
                          )}

                          <Card.Text className="pointsLabel">
                            {item.credit} points
                          </Card.Text>
                        </div>
                      </Box>
                      <Button
                        variant="outlined"
                        color="secondary"
                        className="addcart"
                        onClick={() => {
                          cartCall({
                            products: [{ product: item._id, quantity: 1 }],
                          });
                          infotoast();
                        }}
                        disabled={
                          getCartState.data &&
                          getCartState.data?.products.find(
                            (row) => row.product?._id === item._id
                          )
                            ? true
                            : false
                        }
                      >
                        {getCartState.data &&
                        getCartState.data?.products.find(
                          (row) => row.product?._id === item._id
                        ) ? (
                          <b className="addcartLabel">Déjà ajouté au panier</b>
                        ) : (
                          <b className="addcartLabel">Ajouter au panier</b>
                        )}
                      </Button>{" "}
                    </Card.Body>
                  </Card>
                </div>
              ))}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default HomeContainer;
