import React, { useContext } from "react";
import Button from "@material-ui/core/Button/Button";
import userContext from "../contexts/userContext";
import Navbar from "../layout/Header";
import Footer from "../layout/Footer";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import Img from "../assets/pc.png";
import { getCategories, getSubCategoryRequest } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import useApiState from "../hooks/useApiState";
import Zoom from '@material-ui/core/Fade';
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";

import "./scss/home.scss";

const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  draggable: true,
};

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    backgroundColor: "#00b5cc",
    boxShadow: theme.shadows[1],
    fontSize: 11,
    width: 180,
    height: 35,
    color: "white",
    fontSize: 16,
    display: "flex",
    justifyContent: "center",
  },
}))(Tooltip);
const CategoriesContainer = () => {
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [categoryState, categoryCall] = useApiState(getCategories);
  useDidMount(() => {
    categoryCall();
    subCall();
  });
  console.log(categoryState);
  console.log(subState);

  return (
    <div className="categories-container">
      {categoryState.data &&
        categoryState.data.map((item, index) => (
          <Card
            key={item._id}
            className="cardCat"
            style={{ width: "20rem", height: "14rem" }}
          >
            <Card.Body>
              <div className="cardBd">
                <Card.Img variant="top" className="imgTest" src={item.image} />
                <Card.Text className="firstBody">{item.label}</Card.Text>
                <div className="cardBd2">
                  {subState.data &&
                    subState.data
                      .filter((sub) => sub.category === item._id)
                      .map((sub) => (
                        <LightTooltip
                        title={sub.label}
                        TransitionProps={{ timeout: 700 }}
                        TransitionComponent={Zoom}
                        placement="top"
                      >
                        <div className="thirdBody" key={sub._id}>
                         
                            <div className="third">
                              {" "}
                              <img className="sub-img" src={sub.icon} />{" "}
                            </div>
                        </div>
                        </LightTooltip>

                      ))}
                  <Card.Text className="secondBody">Afficher tout</Card.Text>
                </div>{" "}
              </div>
            </Card.Body>
          </Card>
        ))}
    </div>
  );
};

export default CategoriesContainer;
