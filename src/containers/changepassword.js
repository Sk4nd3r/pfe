import React, { useContext, useState, useEffect } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";
import { toast, zoomn, bounce } from "react-toastify";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Img from "../assets/pc.png";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import TextField from "@material-ui/core/TextField";

import { useForm } from "../hooks/useInputs";
import { updateUser } from "../requests/auth";

import "./scss/profilecontainer.scss";
import "./scss/ordercontainer.scss";

const Password = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [updateUserState, updateUserCall] = useApiState(updateUser);

  const [{ values }, { handleChange }] = useForm({
    initialValues: {
      oldPassword: "",
      password: "",
      confirmPassword: "",
    },
  });
  const [hex, setHex] = useState("#ffffff");

  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  const { user } = useContext(userContext);

  const uri = match.params;
  console.log(uri);
  console.log(categoryState);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    categCall(uri);
    subCall();
    randomizeHex();
  });
  console.log(user, "te");

  console.log(subState, "toto");

  useEffect(() => {
    if (updateUserState.data) {
      toast.success("Votre mot de passe a été mis à jour", { autoClose: 3000 });
      history.goBack();
    }
  }, [updateUserState.data]);

  function handleSubmit() {
    const emptyField = Object.keys(values).find((key) => !values[key]);
    console.log(values);
    if (emptyField)
      toast.error("Vous devez remplir tous les champs obligatoires", {
        autoClose: 3000,
      });
    else if (values.confirmPassword !== values.password) {
      toast.error(
        "Mot de passe et confirmez que le mot de passe ne correspond pass",
        { autoClose: 3000 }
      );
    } else {
      updateUserCall({
        oldPassword: values.oldPassword,
        password: values.password,
      });
    }
  }

  return (
    <div>
      {" "}
      <div className="prof-left">
        <div className="circle-text" style={{ backgroundColor: `${hex}` }}>
          <p className="inside-circle">{user.firstName.substring(0, 1)}</p>
        </div>
        <div>
          <div className="fi-name">
            {" "}
            Bonjour, {user.firstName} {user.lastName}
          </div>
          <div className="la-name"> {user.email}</div>
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Paper className="pap-leftuser">
            <div
              className="wuk"
              onClick={() =>
                history.push({
                  pathname: `/mesorders`,
                })
              }
            >
              {" "}
              <HistoryIcon className="wuk-one" />
              <div className="wuk-two">Historiques des commandes</div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <AccountCircleIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Profile
              </div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <LoyaltyIcon className="wuk-one" />
              <div className="wuk-two">Listes des favoris</div>
            </div>
          </Paper>{" "}
        </Grid>
        <Grid item xs={8} className="pap-right">
          <Paper className="user-right">
            <div className="info-prof">Changer mot de passe</div>{" "}
            <div className="info-all">
              {" "}
              <form autoComplete="off" className="dirc-form">
                <TextField
                  name="oldPassword"
                  value={values.oldPassword}
                  onChange={handleChange}
                  className="inp-profi"
                  label="Actuelle"
                  type="password"
                />
                <TextField
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  className="inp-profi"
                  label="Nouveau"
                  type="password"
                />
                <TextField
                  name="confirmPassword"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  className="inp-profi"
                  label="Confirmer nouveau"
                  type="password"
                />
              </form>
            </div>{" "}
            <div className="twobtns">
              <Button
                variant="contained"
                className="btn-profile"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Retour
              </Button>{" "}
              <Button
                onClick={handleSubmit}
                variant="contained"
                className="btn-passw"
              >
                Valider
              </Button>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Password;
