import React, { useContext, useEffect, useState } from "react";
import { RouteComponentProps, Redirect, Link } from "react-router-dom";
import { Login } from "react-facebook";
import { useDidUpdate } from "../hooks/useLifeCycle";
import { Toast, Alert } from "react-bootstrap";
import { useForm } from "../hooks/useInputs";
import useApiState from "../hooks/useApiState";
import { decodeUri } from "../utils/url";
import userContext from "../contexts/userContext";
import { validateEmail, validatePassword } from "../utils/validation";
import ErrorIcon from "@material-ui/icons/Error";
import Navbar from "../layout/Header";
import {
  Form,
  Button,
  FormControl,
  Nav,
  NavDropdown,
  Col,
  Container,
  Row,
  Dropdown,
} from "react-bootstrap";
import FacebookIcon from "@material-ui/icons/Facebook";
import { toast, zoomn, bounce } from "react-toastify";
import { loginRequest, loginFacebook } from "../requests/auth";
import "./scss/login.scss";
import { setAuthorizationBearer } from "../requests/http";

function LoginFb({ data, handleClick }) {
  const { user, setUser } = useContext(userContext);
  const [loginState, loginCall] = useApiState(loginFacebook);
  useEffect(() => {
    if (data) {
      loginCall({
        facebookId: data.profile.id,
        accessToken: data.tokenDetail.accessToken,
        firstName: data.profile.first_name,
        lastName: data.profile.last_name,
      });
    }
  }, [data]);

  useEffect(() => {
    if (!loginState.fetching && loginState.data) {
      localStorage.setItem("token", JSON.stringify(loginState.data));
      setAuthorizationBearer(loginState.data.token.accessToken);
      setUser(loginState.data.user);
    }
  }, [loginState.data]);

  console.log(loginState.data);
  return (
    <Form.Group className="form3" controlId="formBasicPassword">
      <Button type="button" onClick={handleClick} className="btnCnctFB">
        <FacebookIcon className="fb" /> Se Connecter via Facebook
      </Button>
    </Form.Group>
  );
}

const LoginContainer = ({ location }) => {
  const { user, setUser } = useContext(userContext);
  const errorToast = () => {
    toast("Nom d'utilisateur et/ou Mot de passe invalide!", {
      className: "custom-toast",
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000,
    });
  };

  const [loginState, login] = useApiState(loginRequest);
  const [formState, formActions] = useForm({
    initialValues: {
      email: "",
      password: "",
    },
    validation: {
      email: validateEmail,
      password: validatePassword,
    },
  });
  useDidUpdate(() => {
    if (!loginState.fetching && loginState.data) {
      localStorage.setItem("token", JSON.stringify(loginState.data));
      setAuthorizationBearer(loginState.data.token.accessToken);
      setUser(loginState.data.user);
      console.log(loginState);
    } else if (loginState.errorCode === "USER_WRONG_EMAIL_PASSWORD") {
      errorToast();
    }
  }, [loginState.fetching]);

  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const [showA, setShowA] = useState(false);
  const toggleShowA = () => setShowA(!showA);

  const responseGoogle = (...args) => {
    console.log("response", args);
  };

  if (user) {
    return <Redirect to={user.role === "admin" ? "/admin/home" : "/"} />;
  }

  function onSubmit(e) {
    e.preventDefault();
    if (formActions.validateForm()) {
      login({ email: values.email, password: values.password });
    } else {
      formActions.setTouched({ email: true, password: true });
    }
  }
  return (
    <div className="home width_100">
      <Container>
        <Row>
          <Col>
            <h5 className="text1">CONNECTEZ-VOUS SUR CobeeShop</h5>
            <Form onSubmit={onSubmit}>
              <Form.Group className="form1" controlId="formBasicEmail">
                {touched.error && errors.error && (
                  <div>
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">{errors.error}</span>
                  </div>
                )}
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  className="input"
                  onChange={handleChange}
                  value={values.email}
                  name="email"
                  placeholder="Enter email"
                />
                {touched.email && errors.email && (
                  <div>
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">{errors.email}</span>
                  </div>
                )}
              </Form.Group>
              <Form.Group className="form1" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  className="input"
                  name="password"
                  onChange={handleChange}
                  value={values.password}
                  type="password"
                  placeholder="Password"
                />
                {touched.password && errors.password && (
                  <div>
                    <ErrorIcon className="errorIcon" fontSize="small" />
                    <span className="errorMessage">{errors.password}</span>
                  </div>
                )}
                <Button
                  variant="dark"
                  type="submit"
                  className="btnCnct"
                  onClick={toggleShowA}
                >
                  SE CONNECTER
                </Button>
              </Form.Group>
              {/*  <Toast show={loginState.errorCode === "USER_WRONG_EMAIL_PASSWORD" } onClose={toggleShowA}>
                <Toast.Body>
                  <Toast.Header></Toast.Header>
                  Nom d'utilisateur et/ou Mot de passe invalide!
                </Toast.Body>
              </Toast> */}

              <Form.Group className="form2" controlId="formBasicEmail">
                <Form.Label>Ou utiliser la connexion sociale</Form.Label>
              </Form.Group>

              <Login>
                {({ handleClick, data }) => (
                  <LoginFb handleClick={handleClick} data={data} />
                )}
              </Login>

              <Form.Group className="form3" controlId="formBasicPassword">
                <Button variant="dark" type="button" className="btnCnctGG">
                  Se Connecter via Google
                </Button>
              </Form.Group>
            </Form>{" "}
          </Col>
          <div className="vl"></div>
          <Col>
            <h5 className="text1">CREEZ VOTRE COMPTE</h5>
            <Form.Group className="form4" controlId="formBasicEmail">
              <Form.Label className="lbl1">
                Créer votre compte client CobeeShop
              </Form.Label>
            </Form.Group>
            <Form.Group className="form5" controlId="formBasicEmail">
              <Form.Label>Créer votre</Form.Label>
            </Form.Group>
            <Form.Group className="form5" controlId="formBasicEmail">
              <Link to="/register" className="btnCreer1">
                {" "}
                <Button variant="dark" type="submit" className="btnCreer">
                  CREER VOTRE COMPTE
                </Button>
              </Link>
            </Form.Group>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default LoginContainer;
