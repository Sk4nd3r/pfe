import React, { useContext, useState, useEffect } from "react";
import { Navbar, Form, Nav, Container, Row, Col } from "react-bootstrap";
import userContext from "../contexts/userContext";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import { getProductPerSubCat } from "../requests/product";
import { addToCart } from "../requests/pannel";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import Img from "../assets/pc.png";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import { toast, zoomn, bounce } from "react-toastify";
import FavoriteIcon from "@material-ui/icons/Favorite";
import {
  addToFavorite,
  getFavorite,
  updateFavorite,
} from "../requests/wishlist";
import cartContext from "../contexts/cartContext";

import "./scss/subcategoryContainer.scss";

const SubContainer = ({ history, match, location }) => {
  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));
  const classes = useStyles();

  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [productState, productCall] = useApiState(getProductPerSubCat);
  const [checked, setChecked] = React.useState([]);
  const [cartState, cartCall] = useApiState(addToCart);
  const { getCartState, getCartCall } = useContext(cartContext);

  const [value, setValue] = React.useState(4.5);
  const [favoriteState, favoriteCall] = useApiState(addToFavorite);
  const [getFavoriteState, getFavoriteCall] = useApiState(getFavorite);
  const [updateFavoriteState, updateFavoriteCall] = useApiState(updateFavorite);
  const hearttoast = () => {
    toast.info("vous avez ajouter un produit au liste des favoris", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const uri = match.params;
  const [sort, setSort] = React.useState(1);
  const infotoast = () => {
    toast.info("vous avez ajouter un produit au panier", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const handleChange = (event) => {
    setSort(event.target.value);
  };
  const { user } = useContext(userContext);
  const currentSubCategory =
    subState.data && subState.data.find((sub) => sub._id === uri.id);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    subCall();
    getFavoriteCall();
  });
  useEffect(() => {
    if (!cartState.fetching && cartState.data) {
      getCartCall();
    }
  }, [cartState.fetching]);

  useEffect(() => {
    if (!favoriteState.fetching && favoriteState.data) {
      getFavoriteCall();
    }
  }, [favoriteState.fetching]);
  useEffect(() => {
    if (updateFavoriteState.data) getFavoriteCall();
  }, [updateFavoriteState.data]);
  useEffect(() => {
    let params = uri;
    switch (sort) {
      case 2:
        params = { ...params, sort: "price", order: -1 };
        break;
      case 3:
        params = { ...params, sort: "rating", order: -1 };
        break;
      default:
        params = { ...params, sort: "price", order: 1 };
    }
    if (currentSubCategory) {
      currentSubCategory.characteristics.forEach((char, index) => {
        const value = [];
        if (checked[index]) {
          checked[index].forEach((c, i) => {
            if (c) {
              value.push(char.options[i]);
            }
          });
        }
        if (value.length) params[char.name] = value.join(",");
      });
    }
    productCall(params);
  }, [sort, checked, uri.id]);

  useEffect(() => {
    if (currentSubCategory) {
      setChecked(
        currentSubCategory.characteristics.map((c) =>
          c.options.map(() => false)
        )
      );
    }
  }, [subState.data]);

  console.log(checked);
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={3} className="left-side">
          <Paper className="stat-top">
            {categoryState.data &&
              categoryState.data
                .filter((item) => item._id === currentSubCategory?.category)
                .map((item) => (
                  <div className="top-name" key={item._id}>
                    {item.label}
                  </div>
                ))}

            {subState.data &&
              subState.data
                .filter((sub) => sub.category === currentSubCategory.category)
                .map((sub) => (
                  <div
                    className="top-categ"
                    key={sub._id}
                    onClick={() => {
                      history.push({
                        pathname: `/subcategory/${sub._id}`,
                      });
                    }}
                  >
                    {" "}
                    {currentSubCategory.label === sub.label ? (
                      <div className="crnt-sub">
                        {" "}
                        <ArrowRightIcon className="crnt-arw" />
                        {sub.label}
                      </div>
                    ) : (
                      <div className="others-sub">{sub.label}</div>
                    )}
                  </div>
                ))}
          </Paper>
          {currentSubCategory?.characteristics &&
            currentSubCategory?.characteristics.map((sub, index) => (
              <div key={sub._id}>
                <Paper className="stat-caracone">
                  <div className="ts-name">{sub.name}</div>
                  <div className="direction-under">
                    {" "}
                    {sub.options.map((option, i) => (
                      <FormControlLabel
                        className="mr-charact"
                        control={
                          <Checkbox
                            checked={checked[index] ? checked[index][i] : false}
                            onChange={(e) => {
                              const nextChecked = [...checked];
                              const checkedRow = [...nextChecked[index]];
                              checkedRow[i] = e.target.checked;
                              nextChecked[index] = checkedRow;
                              setChecked(nextChecked);
                            }}
                            inputProps={{ "aria-label": "primary checkbox" }}
                          />
                        }
                        label={option}
                      />
                    ))}
                  </div>
                </Paper>
              </div>
            ))}
        </Grid>
        <Grid item xs={8} className="right-side">
          <div>
            <div>
              <FormControl variant="outlined" className="mtouch">
                <InputLabel>Trié par</InputLabel>
                <Select value={sort} onChange={handleChange}>
                  <MenuItem value={1}>Prix croissant</MenuItem>
                  <MenuItem value={2}>Prix décroissant</MenuItem>
                  <MenuItem value={3}>Les mieux notés</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className="this-products">
              {productState.data &&
                productState.data.map((item, index) => (
                  <div className="card-sub" key={item._id}>
                    <Card className="imgContainer" style={{ width: "15rem" }}>
                      <div>
                        {item.promotion.reduction !== 0 ? (
                          <div className="triangle-img">
                            <div className="triangle-content">
                              -{item.promotion.reduction}%
                            </div>
                          </div>
                        ) : (
                          <div></div>
                        )}
                        <div
                          className="heart-prod"
                          onClick={() => {
                            if (
                              getFavoriteState.data &&
                              !getFavoriteState.data.products.find(
                                (row) => row.product?._id === item._id
                              )
                            ) {
                              favoriteCall({
                                products: [{ product: item._id }],
                              });
                            } else {
                              updateFavoriteCall({
                                products: getFavoriteState.data?.products.filter(
                                  (row, i) => row.product._id !== item._id
                                ),
                              });
                            }
                          }}
                        >
                          {" "}
                          <FavoriteIcon
                            className={
                              getFavoriteState.data &&
                              getFavoriteState.data.products.find(
                                (row) => row.product?._id === item._id
                              )
                                ? "col-heart-active"
                                : "col-heart"
                            }
                          />{" "}
                        </div>
                        <Card.Img
                          className="cardImage"
                          variant="top"
                          src={item.images[0]}
                          onClick={() =>
                            history.push({
                              pathname: `/produit/${item._id}`,
                            })
                          }
                        />
                      </div>
                      {item.label.length > 30 ? (
                        <center className="label-prod">
                          {item.label.substring(0, 27)}...
                        </center>
                      ) : (
                        <center className="label-prod">{item.label}</center>
                      )}

                      <Card.Body className="noPadding">
                        <div className="descriptionContainer">
                          {item.description.length > 50 ? (
                            <Card.Text className="descriptiontext">
                              {" "}
                              {item.description.substring(0, 49)}...{" "}
                            </Card.Text>
                          ) : (
                            <Card.Text className="descriptionother">
                              {" "}
                              {item.description} .
                            </Card.Text>
                          )}
                        </div>
                        <Box
                          component="fieldset"
                          mb={3}
                          borderColor="transparent"
                          className="fieldsetPadding"
                        >
                          <div className="stars">
                            <Rating
                              disabled
                              name="half-rating-read"
                              value={value}
                              precision={0.5}
                              readOnly
                              /*
                          onChange={(event, newValuee) => {
                            setValuee(newValuee);
                          }} */
                              /* onChangeActive={(event, newHover) => {
                            setHover(newHover);
                          }} */
                              className="ratingStars"
                            />{" "}
                            {/*                               {value !== null && <Box ml={2}>{labels[hover !== -1 ? hover : valuee]}</Box>}
                             */}
                            <div className="avisContainer">
                              <b className="avisPts">4.5 </b>{" "}
                              <p className="avis"> d'après 5 Avis</p>
                            </div>
                          </div>
                          <div className="ligne"></div>
                          <div className="pricePoints">
                            {item.promotion.reduction !== 0 ? (
                              <div>
                                <div className="priceLabel">
                                  {item.price -
                                    (item.price * item.promotion.reduction) /
                                      100}{" "}
                                  TND
                                </div>{" "}
                                <div className="priceLabel-promo">
                                  {item.price} TND
                                </div>
                              </div>
                            ) : (
                              <div className="priceLabel">{item.price} TND</div>
                            )}

                            <Card.Text className="pointsLabel">
                              {item.credit} points
                            </Card.Text>
                          </div>
                        </Box>
                        <Button
                          variant="outlined"
                          color="secondary"
                          className="addcart"
                          onClick={() => {
                            cartCall({
                              products: [{ product: item._id, quantity: 1 }],
                            });
                            infotoast();
                          }}
                          disabled={
                            getCartState.data &&
                            getCartState.data?.products.find(
                              (row) => row.product?._id === item._id
                            )
                              ? true
                              : false
                          }
                        >
                          {getCartState.data &&
                          getCartState.data?.products.find(
                            (row) => row.product?._id === item._id
                          ) ? (
                            <b className="addcartLabel">
                              Déjà ajouté au panier
                            </b>
                          ) : (
                            <b className="addcartLabel">Ajouter au panier</b>
                          )}{" "}
                        </Button>{" "}
                      </Card.Body>
                    </Card>
                  </div>
                ))}
            </div>
          </div>{" "}
        </Grid>
      </Grid>
    </div>
  );
};

export default SubContainer;
