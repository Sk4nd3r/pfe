import React, { useContext, useState, useEffect } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import TextField from "@material-ui/core/TextField";

import "./scss/profilecontainer.scss";
import "./scss/ordercontainer.scss";
import { useForm } from "../hooks/useInputs";
import { updateUser } from "../requests/auth";

const Edituser = ({ history, match, location }) => {
  const { user, setUser } = useContext(userContext);
  const [{ values }, { handleChange }] = useForm({
    initialValues: {
      firstName: user?.firstName,
      lastName: user?.lastName,
    },
  });
  const [updateUserState, updateUserCall] = useApiState(updateUser);
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [hex, setHex] = useState("#ffffff");

  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };

  const uri = match.params;

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    categCall(uri);
    subCall();
    randomizeHex();
  });

  useEffect(() => {
    if (updateUserState.data) {
      const data = localStorage.getItem("token");
      if (data) {
        const user = JSON.parse(data);
        localStorage.setItem(
          "token",
          JSON.stringify({ ...user, user: updateUserState.data })
        );
      }
      setUser(updateUserState.data);
      history.push("/profile");
    }
  }, [updateUserState]);

  function onSubmit() {
    updateUserCall(values);
  }

  return (
    <div>
      {" "}
      <div className="prof-left">
        <div className="circle-text" style={{ backgroundColor: `${hex}` }}>
          <p className="inside-circle">{user.firstName.substring(0, 1)}</p>
        </div>
        <div>
          <div className="fi-name">
            {" "}
            Bonjour, {user.firstName} {user.lastName}
          </div>
          <div className="la-name"> {user.email}</div>
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Paper className="pap-leftuser">
            <div
              className="wuk"
              onClick={() =>
                history.push({
                  pathname: `/mesorders`,
                })
              }
            >
              {" "}
              <HistoryIcon className="wuk-one" />
              <div className="wuk-two">Historiques des commandes</div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <AccountCircleIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Profile
              </div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <LoyaltyIcon className="wuk-one" />
              <div className="wuk-two">Listes des favoris</div>
            </div>
          </Paper>{" "}
        </Grid>
        <Grid item xs={8} className="pap-right">
          <Paper className="user-right">
            <div className="info-prof">Modifer les informations de profile</div>{" "}
            <div className="info-all">
              {" "}
              <form autoComplete="off" className="dirc-form">
                <TextField
                  className="inp-profi"
                  label="Nom"
                  name="firstName"
                  value={values.firstName}
                  onChange={handleChange}
                />{" "}
                <TextField
                  className="inp-profi"
                  label="Prenom"
                  name="lastName"
                  value={values.lastName}
                  onChange={handleChange}
                />
              </form>
            </div>{" "}
            <div className="twobtns">
              <Button
                variant="contained"
                className="btn-profile"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Retour
              </Button>{" "}
              <Button
                onClick={onSubmit}
                variant="contained"
                className="btn-passw"
              >
                Valider
              </Button>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Edituser;
