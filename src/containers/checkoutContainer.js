import React, { useState, useEffect, useContext } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { getCartData } from "../requests/pannel";
import { useDidMount } from "../hooks/useLifeCycle";
import {
  AwesomeButton,
  AwesomeButtonProgress,
  AwesomeButtonSocial,
} from "react-awesome-button";
import { Route, Switch, useHistory } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { Icon, InlineIcon } from "@iconify/react";
import diamondIcon from "@iconify/icons-simple-line-icons/diamond";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import ClearIcon from "@material-ui/icons/Clear";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Img from "../assets/pc.png";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { profile } from "../requests/auth";
import { updateToCart } from "../requests/pannel";
import { orderRequest } from "../requests/order";
import { useForm } from "../hooks/useInputs";
import { isStringEmpty, isNumberEmpty } from "../utils/validation";
import Checkbox from "@material-ui/core/Checkbox";
import ErrorIcon from "@material-ui/icons/Error";
import { toast, zoomn, bounce } from "react-toastify";

import "./scss/pannelContainer.scss";
import cartContext from "../contexts/cartContext";
import userContext from "../contexts/userContext";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const Checkout = ({ history }) => {
  const [formState, formActions] = useForm({
    initialValues: {
      adresse: "",
      date: "",
      phone: "",
      points: "",
    },
    validation: {
      label: isStringEmpty,
      adresse: isStringEmpty,
    },
  });
  const { setUser } = useContext(userContext);
  const { getCartState, getCartCall } = useContext(cartContext);
  const { values, errors, touched } = formState;
  const { handleChange, setValues } = formActions;
  const [orderState, orderCall] = useApiState(orderRequest);
  const [profileState, profileCall] = useApiState(profile);
  const [date, setDate] = useState(new Date());
  const [checked, setChecked] = React.useState(false);
  const errorToast = () => {
    toast.error("vérifiez vos points ", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const anotherToast = () => {
    toast.error("vérifiez les toutes les champs ", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  useEffect(() => {
    if (!orderState.fetching && orderState.data) {
      getCartCall();
      profileCall();
      history.push("/")
    }
    if (orderState.errorCode === "Points invalides") {
      errorToast();
    }
  }, [orderState.fetching]);

  useEffect(() => {
    if (profileState.data) {
      const data = localStorage.getItem("token");
      if (data) {
        const user = JSON.parse(data);
        localStorage.setItem(
          "token",
          JSON.stringify({ ...user, user: profileState.data })
        );
      }

      setUser(profileState.data);
    }
  }, [profileState.data]);

  async function onSubmit(e) {
    e.preventDefault();
    const { adresse } = formState.values;
    if (formActions.validateForm()) {
      orderCall({
        ...formState.values,
        date,
      });
      
    } else {
      formActions.setAllTouched(true);
      anotherToast()
    }
  
  }
  return (
    <div className="pag-cr">
      {" "}
      <div className="cont-shop">
        <ExitToAppIcon />{" "}
        <p className="continue-shop" onClick={() => history.push("/panier")}>
          Go back to cart{" "}
        </p>
      </div>
      <div className="ligne-horz"></div>
      <Grid container spacing={3}>
        <Grid item xs={3} className="left-check">
          <Paper className="check-paper">
            {getCartState.data &&
              getCartState.data?.products.map((item, index) => (
                <div className="container-of">
                  <div className="tek_tek">
                    <div className="test-tek">
                      <img className="img-checkout" src={item.product.images} />{" "}
                    </div>
                    <center className="lbl-checkout">
                      {item.product.label}
                    </center>{" "}
                    <div className="ligne-hov"></div>
                  </div>
                </div>
              ))}
          </Paper>
        </Grid>

        <Grid item xs={8} className="right_check">
          <div className="step-one">
            <div className="stp2"></div>
            <div className="stp1"></div>
            <div className="stp3">Vos détails de livraison</div>
          </div>
          <div>
            <form className="form-order">
              {" "}
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked ? checked : false}
                    onChange={(e) => {
                      setChecked(e.target.checked);
                    }}
                    inputProps={{ "aria-label": "primary checkbox" }}
                  />
                }
                label="Utiliser vos points"
                className="supervisor"
              />
              <TextField
                className={checked ? "adrs-order" : "add-disable"}
                label="points à utiliser"
                variant="outlined"
                name="points"
                onChange={handleChange}
                value={values.points}
              />{" "}
              <TextField
                className="adrs-order"
                label="Adresse"
                variant="outlined"
                name="adresse"
                onChange={handleChange}
                value={values.adresse}
              />
              <TextField
                className="adrs-order"
                label="Numéro de téléphone"
                variant="outlined"
                name="phone"
                onChange={handleChange}
                value={values.phone}
              />
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  minDate={new Date()}
                  label="Date"
                  className="check-val"
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  value={date}
                  onChange={(date) => setDate(date)}
                />
              </MuiPickersUtilsProvider>
              <Button
                variant="contained"
                color="secondary"
                className="btn-cenf"
                onClick={onSubmit}
              >
                Envoyer
              </Button>{" "}
            </form>
          </div>
        </Grid>
      </Grid>{" "}
    </div>
  );
};

export default Checkout;
