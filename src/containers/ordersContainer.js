import React, { useContext, useState } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getorder, deleteOrder } from "../requests/order";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Img from "../assets/pc.png";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Moment from "react-moment";
import EditIcon from "@material-ui/icons/Edit";
import Rodal from "rodal";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import Checkbox from "@material-ui/core/Checkbox";

import "./scss/ordercontainer.scss";
import "./scss/profilecontainer.scss";

const OrdersContainer = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [orderState, orderCall] = useApiState(getorder);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [visible, setVisible] = useState(false);
  const [hex, setHex] = useState("#ffffff");

  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  const { user } = useContext(userContext);

  const uri = match.params;
  console.log(uri);
  console.log(categoryState);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    categoryCall();
    orderCall();
    subCall();
    randomizeHex();
  });
  console.log(uri, "toto");
  const show = (id) => {
    setVisible(id);
  };
  const hide = () => {
    setVisible(false);
  };

  function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }

  return (
    <div>
      <div className="prof-left">
        <div className="circle-text" style={{ backgroundColor: `${hex}` }}>
          <p className="inside-circle">{user.firstName.substring(0, 1)}</p>
        </div>
        <div>
          <div className="fi-name">
            {" "}
            Bonjour, {user.firstName} {user.lastName}
          </div>
          <div className="la-name"> {user.email}</div>
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={3} className="">
          <Paper className="pap-leftuser">
            <div className="wuk">
              {" "}
              <HistoryIcon className="wuk-one" />
              <div className="wuk-two">Historiques des commandes</div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <AccountCircleIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Profile
              </div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <LoyaltyIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/liste-favoris`,
                  })
                }
              >
                Listes des favoris
              </div>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={8} className="pap-right">
          {orderState.data?.length === 0 ? (
            <center className="no-order">Vous avez aucune commandes</center>
          ) : (
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className="labels-order">N°.</TableCell>
                    <TableCell align="center" className="labels-order">
                      Id Order
                    </TableCell>
                    <TableCell align="center" className="labels-order">
                      Products
                    </TableCell>
                    <TableCell align="center" className="labels-order">
                      Date
                    </TableCell>
                    <TableCell align="left" className="labels-status">
                      Status
                    </TableCell>
                    <TableCell align="center" className="labels-order">
                      Action
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {orderState.data &&
                    orderState.data.map((row, index) => (
                      <TableRow key={row._id}>
                        <Rodal
                          className="rota-order"
                          animation="slideRight"
                          duration={800}
                          visible={row._id === visible}
                          onClose={hide}
                          width={300}
                          height="auto"
                          overflow="auto"
                          closeOnEsc={true}
                        >
                          <div className="dytra">
                            {row.products.map((p) => {
                              return (
                                <div className="plz-dis">
                                  {" "}
                                  <img
                                    src={p.product.images[0]}
                                    className="img-day"
                                  />{" "}
                                  <center className="tit-rodl">
                                    {p.product.label}
                                  </center>
                                </div>
                              );
                            })}
                          </div>
                        </Rodal>
                        <TableCell component="th" scope="row">
                          {index + 1}
                        </TableCell>
                        <TableCell align="center" className="id-position">
                          {row._id.substring(18, 24)}
                        </TableCell>
                        <TableCell align="center" className="cons-position">
                          {" "}
                          <Button
                            variant="outlined"
                            size="small"
                            color="primary"
                            onClick={() => show(row._id)}
                          >
                            Voir vos produits
                          </Button>
                        </TableCell>
                        <TableCell align="center">
                          {" "}
                          <Moment format="YYYY/MM/DD">{row.date}</Moment>
                        </TableCell>
                        <TableCell align="center" className="status-position">
                          {row.status === "not processed" ? (
                            <div className="w3-containerbad">
                              <div className="w3-border">
                                <div className="w3-grey"></div>
                              </div>
                              <div className="w3-circle"></div>
                              <div className="w3-text"> {row.status}</div>
                            </div>
                          ) : (
                            <div className="w3-container">
                              <div className="w3-border">
                                <div className="w3-greysuccess"></div>
                              </div>
                              <div className="w3-circlesuccess"></div>
                              <div className="w3-text"> {row.status}</div>
                            </div>
                          )}
                        </TableCell>
                        <TableCell align="center" className="del-position">
                          {row.status === "not processed" ? (
                            <div className="ides-tool">
                              <Tooltip
                                title="Supprimer"
                                placement="left"
                                className="order-delt"
                              >
                                <IconButton aria-label="delete">
                                  <DeleteIcon
                                    onClick={() =>
                                      deleteOrder({ id: row._id }).then(() => {
                                        orderCall();
                                      })
                                    }
                                  />
                                </IconButton>
                              </Tooltip>
                            </div>
                          ) : (
                            <div></div>
                          )}
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default OrdersContainer;
