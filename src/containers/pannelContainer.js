import React, { useState, useEffect, useContext } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { getCartData } from "../requests/pannel";
import { useDidMount } from "../hooks/useLifeCycle";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  AwesomeButton,
  AwesomeButtonProgress,
  AwesomeButtonSocial,
} from "react-awesome-button";
import { Route, Switch, useHistory } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { Icon, InlineIcon } from "@iconify/react";
import diamondIcon from "@iconify/icons-simple-line-icons/diamond";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import ClearIcon from "@material-ui/icons/Clear";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Shopimg from "../assets/shop.png";
import { updateToCart } from "../requests/pannel";
import Img from "../assets/pc.png";
import SaveIcon from "@material-ui/icons/Save";
import Tooltip from "@material-ui/core/Tooltip";

import "./scss/pannelContainer.scss";
import cartContext from "../contexts/cartContext";

const Pannel = () => {
  const { getCartState, getCartCall } = useContext(cartContext);
  const [updateCartState, updateCartCall] = useApiState(updateToCart);
  const history = useHistory();
  const [count, setCount] = useState([]);
  const [categoryState, categoryCall] = useApiState(getCategories);
  useDidMount(() => {
    categoryCall();
    getCartCall();
  });
  useEffect(() => {
    if (getCartState.data) {
      setCount(getCartState.data.products.map((product) => product.quantity));
    }
  }, [getCartState.data]);
  useEffect(() => {
    if (updateCartState.data) getCartCall();
  }, [updateCartState.data]);
  let sum = 0;

  getCartState.data &&
    getCartState.data.products.forEach((item, index) => {
      if (item.product.promotion.reduction !== 0) {
        sum =
          sum +
          (item.product.price -
            (item.product.price * item.product.promotion.reduction) / 100) *
            count[index];
      } else {
        sum = sum + item.product.price * count[index];
      }
    });

  console.log(getCartState.data?.products.length, "this is it");

  console.log(getCartState.data?.products.length, "forever");
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  const card = [
    {
      test: "",
    },
  ];
  return (
    <div className="page-container">
      {" "}
      <div className="cont-shop">
        <ExitToAppIcon />{" "}
        <p className="continue-shop" onClick={() => history.push("/")}>
          Continuer vos achats
        </p>
      </div>
      <div className="ligne-horz"></div>
      {getCartState.data?.products &&
      getCartState.data?.products.length === 0 ? (
        <div>
          <center className="title-pannel">
            Produits dans votre panier ({getCartState.data?.products.length})
          </center>{" "}
          <div>
            <div className="second-ligne"></div>{" "}
            <div className="fri-cen">
              <div>
                <img src={Shopimg} />
              </div>
              <p className="title-pannel">VOTRE PANIER EST VIDE</p>{" "}
              <p className="title-panel" onClick={() => history.push("/")}>
                Faire du shopping
              </p>
            </div>
          </div>
          <div className="last-ligne2"></div>
        </div>
      ) : (
        <div>
          <center className="title-pannel">
            Produits dans votre panier ({getCartState.data?.products.length})
          </center>{" "}
          <div className="pannelContainer">
            <div className="second-ligne"></div>
          </div>
          {getCartState.data &&
            getCartState.data?.products.map((item, index) => (
              <div key={item._id}>
                {" "}
                {/*  //2nd */}
                <div className="prod-pannel">
                  <div className="prod-second">
                    {" "}
                    <img
                      className="imgpannel"
                      onClick={() =>
                        history.push({
                          pathname: `/produit/${item._id}`,
                        })
                      }
                      src={item.product.images}
                    />
                    <div className="desc-pannel">
                      {" "}
                      <p className="another-title">{item.product.label}</p>{" "}
                      <div className="anotherspace">
                        <p className="another-desc">
                          {item.product.description}
                        </p>
                        <p className="space">
                          {count[index] !== undefined && (
                            <div className="quantity-input">
                              <button
                                className="quantity-input__modifier quantity-input__modifier--left my-step"
                                onClick={() => {
                                  const anotherCount = [...count];
                                  const stayCount = [...count];

                                  anotherCount[index] = anotherCount[index] - 1;
                                  stayCount[index] = stayCount[index] - 0;

                                  if (anotherCount[index] < 1) {
                                    setCount(stayCount);
                                  } else {
                                    setCount(anotherCount);
                                  }
                                  if (getCartState.data)
                                    updateCartCall({
                                      products: getCartState.data.products.map(
                                        (product, index) => ({
                                          product: product.product._id,
                                          quantity: Number(anotherCount[index]),
                                        })
                                      ),
                                    });
                                }}
                              >
                                &mdash;
                              </button>
                              <input
                                className="quantity-input__screen"
                                type="text"
                                value={count[index]}
                                readonly
                              />
                              <button
                                onClick={() => {}}
                                className="quantity-input__modifier quantity-input__modifier--right my-step"
                                onClick={() => {
                                  const anotherCount = [...count];
                                  anotherCount[index] = anotherCount[index] + 1;
                                  setCount(anotherCount);
                                  if (getCartState.data)
                                    updateCartCall({
                                      products: getCartState.data.products.map(
                                        (product, index) => ({
                                          product: product.product._id,
                                          quantity: Number(anotherCount[index]),
                                        })
                                      ),
                                    });
                                }}
                              >
                                &#xff0b;
                              </button>
                            </div>
                          )}
                        </p>
                        {item.product.promotion.reduction !== 0 ? (
                          <div className="both-prix">
                            {" "}
                            <div className="pricePannel">
                              {item.product.quantity !== 1
                                ? (item.product.price -
                                    (item.product.price *
                                      item.product.promotion.reduction) /
                                      100) *
                                  count[index]
                                : item.product.price}{" "}
                              TND
                            </div>
                            <div className="new-pricePan ">
                              {item.product.price} TND
                            </div>
                          </div>
                        ) : (
                          <div>
                            <div className="pricePannel">
                              {item.product.price * count[index]} TND
                            </div>
                          </div>
                        )}
                        <Tooltip
                          title="Supprimer"
                          placement="bottom"
                          className="order-delt"
                        >
                          <DeleteIcon
                            onClick={() => {
                              updateCartCall({
                                products: getCartState.data.products.filter(
                                  (item, i) => index !== i
                                ),
                              });
                            }}
                            className="save-co"
                          />
                        </Tooltip>
                      </div>
                      <div className="another-points">
                        {" "}
                        <Icon className="dia-prod" icon={diamondIcon} />
                        <p>{item.product.credit}</p>
                        <p className="another-favoris">
                          {" "}
                          <FavoriteBorderIcon
                            fontSize="small"
                            className="another-heart"
                          />
                          Ajouter à la liste de favoris
                        </p>
                      </div>
                    </div>
                  </div>{" "}
                </div>{" "}
                <div className="another-one">
                  <div className="mid-ligne"></div>
                </div>
              </div>
            ))}
          <div className="pannelContainer">
            <div className="last-ligne2"></div>
          </div>{" "}
          <p className="sumLabel">Total: {sum} TND</p>
          <div
            className="awsm-container"
            onClick={() => {
              if (getCartState.data)
                updateCartCall({
                  products: getCartState.data.products.map(
                    (product, index) => ({
                      product: product.product._id,
                      quantity: Number(count[index]),
                    })
                  ),
                });
              updateCartCall({ total: (getCartState.data.total = sum) });
            }}
          >
            <AwesomeButtonProgress
              type="pinterest awsm-btn hover pressure"
              action={() =>
                history.push({
                  pathname: "/checkout",
                })
              }
            >
              Check-Out
            </AwesomeButtonProgress>
          </div>
        </div>
      )}
      <div className="emptyDiv"></div>
    </div>
  );
};

export default Pannel;
