import React, { useContext, useState, useEffect } from "react";
import Button from "@material-ui/core/Button/Button";
import userContext from "../contexts/userContext";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { useDidMount, useDidUpdate } from "../hooks/useLifeCycle";
import useApiState from "../hooks/useApiState";
import FlagIcon from "@material-ui/icons/Flag";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import FavoriteIcon from "@material-ui/icons/Favorite";

import {
  getProductId,
  addReview,
  getReview,
  updateReview,
  deleteReview,
} from "../requests/product";
import { addToCart } from "../requests/pannel";
import Moment from "react-moment";
import { decodeUri } from "../utils/url";
import { toast, zoomn, bounce } from "react-toastify";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { Icon, InlineIcon } from "@iconify/react";
import diamondIcon from "@iconify/icons-simple-line-icons/diamond";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import TextareaAutosize from "@material-ui/core/TextField/TextField";
import EmptyIcon from "../assets/empty.png";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircleRounded";
import commentContext from "../contexts/commentContext";
import NotInterestedIcon from "@material-ui/icons/NotInterested";
import { useForm } from "../hooks/useInputs";
import {
  validateEmail,
  validatePassword,
  isStringEmpty,
} from "../utils/validation";
import SearchBar from "material-ui-search-bar";
import Delete from "@material-ui/icons/Delete";
import Edit from "@material-ui/icons/Edit";
import Rodal from "rodal";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TextareaAutosizes from "@material-ui/core/TextareaAutosize";
import {
  addToFavorite,
  getFavorite,
  updateFavorite,
} from "../requests/wishlist";
import { addReport } from "../requests/report";
import cartContext from "../contexts/cartContext";
import Replies from "../components/replies";
import TextField from "@material-ui/core/TextField/TextField";
import "./scss/product.scss";

const ProductContainer = ({ history, match, location }) => {
  const { user } = useContext(userContext);
  const { from } = decodeUri(location.search);
  const [value, setValue] = React.useState(0);
  const [valueProd, setValueProd] = React.useState(0);
  const [reply, setReply] = useState("");
  const [review, setReview] = useState("");
  const [report, setReport] = useState("");
  const [type, setType] = useState("");
  const [reason, setReason] = useState("");
  const [view, setView] = useState(false);
  const toggleView = () => setView(!view);
  const [edit, setEdit] = useState(false);
  const toggleReply = () => setReply(!reply);
  const errorcmnTaost = () => {
    toast.info("Vous devez ajouter une note au produit", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 3000,
    });
  };
  const uri = match.params;
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const [reviewState, reviewCall] = useApiState(addReview);
  const { getReviewsState, getReviewsCall } = useContext(commentContext);
  const [cartState, cartCall] = useApiState(addToCart);
  const [visible, setVisible] = useState("");

  const [productState, productCall] = useApiState(getProductId);
  const [updateReviewState, updateReviewCall] = useApiState(updateReview);
  const [deleteReviewState, deleteReviewCall] = useApiState(deleteReview);
  const [addReportState, addReportCall] = useApiState(addReport);
  const { getCartState, getCartCall } = useContext(cartContext);

  const [favoriteState, favoriteCall] = useApiState(addToFavorite);
  const [getFavoriteState, getFavoriteCall] = useApiState(getFavorite);
  const [updateFavoriteState, updateFavoriteCall] = useApiState(updateFavorite);
  const userReview = getReviewsState.data?.find(
    (r) => user?._id === r.user._id
  );
  const infotoast = () => {
    toast.info("vous avez ajouter ce produit au panier", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 5000,
    });
  };
  const hearttoast = () => {
    toast.info("vous avez ajouter un produit au liste des favoris", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 2000,
    });
  };
  const removeheartToast = () => {
    toast.info("vous avez retier un produit du la liste des favoris", {
      draggable: true,
      postion: toast.POSITION.BOTTOM_CENTER,
      autoClose: 2000,
    });
  };
  console.log(uri);

  const show = (id) => {
    setVisible(id);
  };
  const hide = () => {
    setVisible("");
  };

  useEffect(() => {
    if (edit && userReview) {
      setReview(userReview.review);
    }
  }, [edit]);

  useDidMount(() => {
    getFavoriteCall();
  });
  useEffect(() => {
    if (!cartState.fetching && cartState.data) {
      getCartCall();
    }
  }, [cartState.fetching]);
  useEffect(() => {
    if (addReportState.data) {
      toast.success("Votre signal a été ajouté");
      setVisible("");
    }
  }, [addReportState.data]);

  useEffect(() => {
    if (productState.data) setValue(productState.data.rating);
  }, [productState.data]);

  useEffect(() => {
    if (!favoriteState.fetching && favoriteState.data) {
      getFavoriteCall();
    }
  }, [favoriteState.fetching]);

  useEffect(() => {
    if (updateFavoriteState.data) getFavoriteCall();
  }, [updateFavoriteState.data]);

  useDidUpdate(() => {
    if (!reviewState.fetching) {
      getReviewsCall(uri);
    }
  }, [reviewState.fetching]);

  useDidUpdate(() => {
    if (!updateReviewState.fetching) {
      getReviewsCall(uri);
    }
  }, [updateReviewState.fetching]);

  useDidUpdate(() => {
    if (!deleteReviewState.fetching) {
      getReviewsCall(uri);
    }
  }, [deleteReviewState.fetching]);

  useEffect(() => {
    productCall(uri);
    getReviewsCall(uri);
  }, [uri.id]);

  useEffect(() => {
    if (userReview) setValueProd(userReview.rating);
  }, [getReviewsState.data]);

  return (
    <div className=" width_100">
      <Grid container>
        {" "}
        <Rodal
          className="rota-order"
          animation="slideRight"
          duration={800}
          visible={visible}
          onClose={hide}
          width={700}
          height={345}
          closeOnEsc={true}
        >
          <div>
            <center className="tit-rodl">Rapport</center>
          </div>

          <div className="sct-signl">
            <Select
              value={type}
              onChange={(e) => setType(e.target.value)}
              className="select-area"
              name="type"
            >
              {[
                "langage injurieux",
                "Critique impartiale",
                "Spam",
                "Publicité",
                "Autre",
              ].map((v) => (
                <MenuItem value={v}>{v}</MenuItem>
              ))}
            </Select>
            {/*  <FormControl>
              <InputLabel id="demo-simple-select-helper-label">
                Raison
              </InputLabel>
              <Select
              className="resi-sgl"
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={value}
                onChange={(e) => setReport(e.target.value)}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
              <FormHelperText>Raison de rapport</FormHelperText>
            </FormControl> */}
          </div>

          <div className="report-modal-container">
            <TextareaAutosizes
              value={reason}
              onChange={(e) => setReason(e.target.value)}
              className="sig-area"
              rowsMin={4}
              rowsMax={4}
              width={400}
              type="text"
              name=""
              placeholder="Message(facultatif)"
            />
          </div>
          <div className="sct-signl">
            <Button
              variant="contained"
              color="secondary"
              disabled={!type}
              onClick={() => addReportCall({ id: visible, type, reason })}
            >
              Envoyer
            </Button>
          </div>
        </Rodal>
        <Grid item xs={12} sm={6} className="imgproduits">
          <img className="imgP" src={productState.data?.images[0]}></img>
        </Grid>
        <Grid item xs={12} sm={6}>
          <div className="titlePrice">
            {" "}
            <p className="productLabel">{productState.data?.label} </p>{" "}
            {productState.data?.promotion.reduction !== 0 ? (
              <div>
                <h4 className="prix">
                  {productState.data?.price -
                    (productState.data?.price *
                      productState.data?.promotion.reduction) /
                      100}{" "}
                  TND
                </h4>{" "}
                <h4 className="isnthere">{productState.data?.price} TND</h4>
              </div>
            ) : (
              <div className="prix">{productState.data?.price} TND</div>
            )}
          </div>
          <div className="containerproduct">
            <div className="desc">
              <b className="desclabel">Description :</b>{" "}
              <p className="mydesc">{productState.data?.description}</p>
            </div>
            <div className="desc">
              <b className="desclabel">Caractéristiques :</b>{" "}
              <p className="mydesc">
                {productState.data?.characteristics.map((option) => (
                  <div>
                    {productState.data?.characteristics.length === 1 ? (
                      <div>
                        {option.label}:{option.value}
                      </div>
                    ) : (
                      <div className="fri-direct">
                        {option.label}:{option.value}
                      </div>
                    )}
                  </div>
                ))}
              </p>
            </div>
            {productState.data?.quantity !== 0 ? (
              <div className="desc2">
                <p>
                  <b className="desclabel">Desponibilité :</b> En stock{" "}
                  <CheckCircleIcon className="check" />
                </p>
              </div>
            ) : (
              <div className="desc2">
                <p>
                  <b className="desclabel">Desponibilité :</b> non disponible{" "}
                  <NotInterestedIcon className="checknot" />
                </p>
              </div>
            )}

            <div className="desc3">
              <p>
                <b className="desclabel">Points :</b>{" "}
                {productState.data?.credit} points{" "}
                <Icon className="diamondprod" icon={diamondIcon} />
              </p>
            </div>
            {productState.data?.promotion.reduction !== 0 ? (
              <div className="desc3">
                <p>
                  <b className="desclabel">Reduction :</b> -
                  {productState.data?.promotion.reduction} %{" "}
                </p>
              </div>
            ) : (
              <div></div>
            )}
            <div className="desc4">
              <Button
                variant="outlined"
                color="secondary"
                className="addcartprod"
                onClick={() => {
                  cartCall({
                    products: [
                      { product: productState.data?._id, quantity: 1 },
                    ],
                  });
                  infotoast();
                }}
                disabled={
                  getCartState.data &&
                  getCartState.data?.products.find(
                    (row) => row.product?._id === productState.data?._id
                  )
                    ? true
                    : false
                }
              >
                {getCartState.data &&
                getCartState.data?.products.find(
                  (row) => row.product?._id === productState.data?._id
                ) ? (
                  <b className="addcartLabelprod">
                    Déja ajouter au panier{" "}
                    <AddShoppingCartIcon className="shopcarticon" />
                  </b>
                ) : (
                  <b className="addcartLabelprod">
                    Ajouter au panier{" "}
                    <AddShoppingCartIcon className="shopcarticon" />
                  </b>
                )}
              </Button>{" "}
              <div
                className="addfavoris"
                onClick={() => {
                  if (
                    getFavoriteState.data &&
                    !getFavoriteState.data?.products.find(
                      (row) => row.product._id === productState.data?._id
                    )
                  ) {
                    favoriteCall({
                      products: [{ product: productState.data?._id }],
                    });
                    hearttoast();
                  } else {
                    updateFavoriteCall({
                      products: getFavoriteState.data?.products.filter(
                        (row, i) => row.product._id !== productState.data?._id
                      ),
                    });
                    removeheartToast();
                  }
                }}
              >
                {getFavoriteState.data &&
                getFavoriteState.data?.products.find(
                  (row) => row.product._id === productState.data?._id
                ) ? (
                  <p className="favoris">
                    {" "}
                    <FavoriteIcon className="heartcovrer" />
                    retirer de la liste de favoris
                  </p>
                ) : (
                  <p className="favoris">
                    {" "}
                    <FavoriteBorderIcon className="heart" />
                    Ajouter à la liste de favoris
                  </p>
                )}
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
      <Grid container className="aviscontainerprod">
        <div className="aviscl">
          <div className="avisprod">
            <p className="avislabel">Avis des utilisateurs</p>
          </div>
          <div className="cmnts">
            <div className="cmntcontainer">
              <div>
                <p className="cmntlabel">
                  Commentaires ({getReviewsState.data?.length})
                </p>
              </div>
              <div className="first_cmnt cmt_container">
                {getReviewsState.data && getReviewsState.data.length !== 0 ? (
                  getReviewsState.data.map((item) => {
                    const isWriter = user?._id === item.user._id;

                    return (
                      <div className="review_container">
                        <div className="first_cmnt" key={item._id}>
                          <div className="tireCmnt">
                            <AccountCircleRoundedIcon
                              fontSize="medium"
                              className="user-icon"
                            />{" "}
                            {item.user.firstName} {item.user.lastName} .{" "}
                            <p className="date-cmnt">
                              <Moment format="YYYY/MM/DD">
                                {item.postedOn}
                              </Moment>{" "}
                            </p>
                          </div>
                          {edit && isWriter ? (
                            <TextareaAutosize
                              className={"type-reply-hide"}
                              rowsMin={1.3}
                              placeholder="Write a reply..."
                              value={review}
                              onChange={(e) => setReview(e.target.value)}
                              onKeyDown={(e) => {
                                if (e.key === "Enter" && review) {
                                  e.preventDefault();
                                  updateReviewCall({
                                    id: item._id,
                                    review,
                                  });
                                  setReview("");
                                  setEdit(false);
                                }
                                if (e.key === "Enter" && reviewState.errors) {
                                  errorcmnTaost();
                                }
                              }}
                            />
                          ) : (
                            <p className="thiscmnt">{item.review}</p>
                          )}
                          <Replies id={item._id} />
                          <div className="sig-edit">
                            {!isWriter && (
                              <div
                                className="signl-rep"
                                onClick={() => show(item._id)}
                              >
                                <FlagIcon
                                  fontSize="small"
                                  className="flag-cmnt"
                                />
                                Signaler
                              </div>
                            )}
                            {isWriter && (
                              <div className="pts-edit">
                                <MoreHorizIcon
                                  className="del-ed"
                                  onClick={handleClick}
                                />
                                <Menu
                                  id="simple-menu"
                                  anchorEl={anchorEl}
                                  keepMounted
                                  open={Boolean(anchorEl)}
                                  onClose={handleClose}
                                >
                                  <MenuItem onClick={handleClose}>
                                    <div
                                      onClick={() => setEdit(!edit)}
                                      color="primary"
                                    >
                                      Modifer
                                    </div>
                                  </MenuItem>
                                  <MenuItem onClick={handleClose}>
                                    {" "}
                                    <div
                                      onClick={() =>
                                        deleteReviewCall({ id: item._id })
                                      }
                                      color="error"
                                    >
                                      Supprimer
                                    </div>
                                  </MenuItem>
                                </Menu>
                                {/*
                                 */}
                              </div>
                            )}
                          </div>
                          {item._id === reply && (
                            <TextareaAutosize
                              aria-label="minimum height"
                              rowsMin={1.3}
                              placeholder="Write a reply..."
                            />
                          )}
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <center className="null-cmnt">
                    {" "}
                    Aucune commentaire pour ce produit{" "}
                  </center>
                )}
              </div>

              <div className="text-comnt">
                {!getReviewsState.data?.find(
                  (r) => user?._id === r.user?._id
                ) && (
                  <TextareaAutosize
                    aria-label="minimum height"
                    className="type-cmnt"
                    rowsMin={1.3}
                    placeholder="Write a comment..."
                    onKeyDown={(e) => {
                      if (e.key === "Enter" && review) {
                        e.preventDefault();
                        reviewCall({
                          id: uri.id,
                          data: { review, rating: valueProd },
                        });
                        setReview("");
                      }
                    }}
                    value={review}
                    onChange={(event) => setReview(event.target.value)}
                  />
                )}
              </div>
            </div>
            <div className="votecontainer">
              {" "}
              <div>
                <div className="new_line">
                  {" "}
                  <p className="evallabel">Evaluation (8 avis)</p>
                </div>
                <div>
                  <Box
                    component="fieldset"
                    className="first_line"
                    mb={3}
                    borderColor="transparent"
                  >
                    <center className="evallabel1">
                      {" "}
                      {productState.data?.rating || 0}/5
                    </center>
                    <Rating
                      name="half-rating-read"
                      className="rateIt"
                      value={value}
                      precision={0.5}
                      readOnly
                      disabled
                    />
                  </Box>
                </div>
                <div className="new_line">
                  {" "}
                  <p className="evallabel">Ajouter votre evaluation</p>
                </div>
                <div className="rateIt">
                  <Box component="fieldset" mb={3} borderColor="transparent">
                    <Rating
                      name="simple-controlled"
                      value={valueProd}
                      onChange={(event, newValueProd) => {
                        setValueProd(newValueProd);
                      }}
                    />
                  </Box>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Grid>

      <Grid container className="bigger">
        <div className="avis">
          <div className="centeravis">
            <p className="avislabel"></p>
          </div>{" "}
        </div>
      </Grid>
    </div>
  );
};

export default ProductContainer;
