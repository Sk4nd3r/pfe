import React, { useContext, useState, useEffect } from "react";
import {
  Navbar,
  Form,
  FormControl,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userContext from "../contexts/userContext";

import { Link, withRouter } from "react-router-dom";
import { getCategories } from "../requests/categories";
import { useDidMount } from "../hooks/useLifeCycle";
import { Card, Carousel } from "react-bootstrap";
import Slider from "react-slick";
import useApiState from "../hooks/useApiState";
import Button from "@material-ui/core/Button";
import { getCateg } from "../requests/categories";
import { getSubCategoryRequest } from "../requests/categories";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Img from "../assets/pc.png";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import DeleteIcon from "@material-ui/icons/Delete";
import Tooltip from "@material-ui/core/Tooltip";
import { updateToCart } from "../requests/pannel";
import cartContext from "../contexts/cartContext";

import {
  addToFavorite,
  getFavorite,
  updateFavorite,
} from "../requests/wishlist";

import "./scss/profilecontainer.scss";
import "./scss/ordercontainer.scss";

const Favorite = ({ history, match, location }) => {
  const [categoryState, categoryCall] = useApiState(getCategories);
  const [categState, categCall] = useApiState(getCateg);
  const [subState, subCall] = useApiState(getSubCategoryRequest);
  const [hex, setHex] = useState("#ffffff");
  const [getFavoriteState, getFavoriteCall] = useApiState(getFavorite);
  const { getCartState, getCartCall } = useContext(cartContext);
  const [updateFavoriteState, updateFavoriteCall] = useApiState(updateFavorite);
  const [favoriteState, favoriteCall] = useApiState(addToFavorite);

  const randomizeHex = () => {
    let randomColor = [
      "red",
      "blue",
      "green",
      "purple",
      "#0091ea",
      "#ff5722",
      "#d500f9",
      "#ffc400",
    ];
    setHex(randomColor[Math.floor(Math.random() * randomColor.length)]);
  };
  const { user } = useContext(userContext);

  const uri = match.params;
  console.log(uri);
  console.log(categoryState);
  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: true,
  };
  useDidMount(() => {
    subCall();
    randomizeHex();
    getCartCall();
    getFavoriteCall();
  });
  console.log(user, "te");

  useEffect(() => {
    if (!favoriteState.fetching && favoriteState.data) {
      getFavoriteCall();
    }
  }, [favoriteState.fetching]);
  useEffect(() => {
    if (updateFavoriteState.data) getFavoriteCall();
  }, [updateFavoriteState.data]);
  console.log(getFavoriteState.data, "toto");

  return (
    <div>
      {" "}
      <div className="prof-left">
        <div className="circle-text" style={{ backgroundColor: `${hex}` }}>
          <p className="inside-circle">{user.firstName.substring(0, 1)}</p>
        </div>
        <div>
          <div className="fi-name">
            {" "}
            Bonjour, {user.firstName} {user.lastName}
          </div>
          <div className="la-name"> {user.email}</div>
        </div>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Paper className="pap-leftuser">
            <div
              className="wuk"
              onClick={() =>
                history.push({
                  pathname: `/mesorders`,
                })
              }
            >
              {" "}
              <HistoryIcon className="wuk-one" />
              <div className="wuk-two">Historiques des commandes</div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <AccountCircleIcon className="wuk-one" />
              <div
                className="wuk-two"
                onClick={() =>
                  history.push({
                    pathname: `/profile`,
                  })
                }
              >
                Profile
              </div>
            </div>{" "}
            <div className="wuk">
              {" "}
              <LoyaltyIcon className="wuk-one" />
              <div className="wuk-two">Listes des favoris</div>
            </div>
          </Paper>{" "}
        </Grid>
        <Grid item xs={8} className="pap-right">
          <Paper className="user-fav">
            <div className="info-prof">Vos produits les plus favoris</div>{" "}
            {getFavoriteState.data?.products &&
            getFavoriteState.data?.products.length === 0 ? (
              <center>Vous avez aucun produit dans la liste des favoris</center>
            ) : (
              <div>
                {getFavoriteState.data &&
                  getFavoriteState.data?.products.map((item, index) => (
                    <div className="row-directi">
                      <div className="col-directi">
                        <div className="border-fav">
                          <img
                            src={item.product?.images[0]}
                            className="img-favo"
                          />
                        </div>
                        <p className="tex-fav">{item.product?.label}</p>{" "}
                        <p className="pri-fav">
                          {item.product?.price -
                            (item.product?.price *
                              item.product?.promotion.reduction) /
                              100}{" "}
                          TND
                        </p>{" "}
                        <div className="fav-delt">
                          {" "}
                          <Tooltip title="Supprimer" placement="bottom">
                            <DeleteIcon
                              onClick={() => {
                                updateFavoriteCall({
                                  products: getFavoriteState.data?.products.filter(
                                    (row, i) => row.product._id !== item._id
                                  ),
                                });
                              }}
                              className="save-co"
                            />
                          </Tooltip>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            )}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Favorite;
