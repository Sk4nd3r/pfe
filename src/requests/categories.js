import {
  axiosPostFilesData,
  axiosPost,
  axiosGet,
  axiosDelete,
  axiosPatch,
} from "./http";

export const addCategoryRequest = (data) =>
  axiosPost("/categories/createcategory", { data });
export const getCategories = (params) =>
  axiosGet("/categories/listcategories", { params });
export const deleteCategorie = (id) =>
  axiosDelete("/categories/delete/" + id.id);
export const addSubCategoryRequest = (data) =>
  axiosPost("/subcategories/create", { data });
export const getSubCategoryRequest = (id) =>
  axiosGet("/subcategories/list", { params: { category: id } });
export const getCateg = (id) => axiosGet("/categories/list/" + id.id);
export const deleteSub = (id) => axiosDelete("/subcategories/delete/" + id.id);

export const upload = (file) => {
  const formData = new FormData();
  formData.append("image", file);
  return axiosPostFilesData("/upload", { data: formData });
};

export const getPopularCategories = () => axiosGet("/categories/popular");

export const updateCategory = ({ id, ...data }) =>
  axiosPatch("/categories/update/" + id, { data });

export const updateSubCategoryRequest = ({ id, ...data }) =>
  axiosPatch("/subcategories/update/" + id, { data });
