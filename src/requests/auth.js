import { axiosPost, axiosGet, axiosPatch } from './http';
export const loginRequest = (data) => axiosPost('/auth/login', { data });
export const refreshRequest = (data) => axiosPost('/auth/refresh-token', { data });
export const registerRequest = (data) => axiosPost('/auth/register', { data });
export const getUsers = (params) => axiosGet('/users', { params });

export const profile = () => axiosGet('/users/profile');

export const updateUser = (data) => axiosPatch('/users/', { data });

export const loginFacebook = (data) => axiosPost('/auth/facebook', { data });
