import {
  axiosPostFilesData,
  axiosPost,
  axiosGet,
  axiosDelete,
  axiosPatch,
} from "./http";

export const orderRequest = (data) => axiosPost("/order/create", { data });
export const getorder = (params) => axiosGet("/order/list", { params });
export const getAllorders = (params) => axiosGet("/order/listall", { params });
export const deleteOrder = (id) => axiosDelete("/order/delete/" + id.id);
export const updateOrder = ({ id, ...data }) =>
  axiosPatch("/order/update/" + id, { data });
export const getDailySales = () => axiosGet("/order/daily-sales");
export const getEarnings = () => axiosGet("/order/earnings");
