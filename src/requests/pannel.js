import { axiosPostFilesData, axiosPost, axiosGet, axiosPatch } from "./http";

export const addToCart = (data) => axiosPost("/cart/"  ,{ data });
export const getCartData = (params) => axiosGet("/cart/"  ,{ params });
export const updateToCart = (data) => axiosPatch("/cart/"  ,{ data });
