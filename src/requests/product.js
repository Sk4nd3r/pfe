import {
  axiosPostFilesData,
  axiosPost,
  axiosGet,
  axiosDelete,
  axiosPatch,
} from "./http";

export const addProductRequest = (data) =>
  axiosPost("/products/create", { data });
export const getProductRequest = (params) =>
  axiosGet("/products/list", { params });
export const getProductId = (id) => axiosGet("/products/list/" + id.id);
export const getProductPerSubCat = ({ id, ...params }) =>
  axiosGet("/products/listPerSubCat/" + id, { params });

export const getMostPopular = (params) =>
  axiosGet("/products/listpopulars", { params });
export const getRecommanded = (params) =>
  axiosGet("/recommanded/list", { params });
export const deleteProduit = (id) => axiosDelete("/products/delete/" + id.id);

export const upload = (file) => {
  const formData = new FormData();
  formData.append("image", file);
  return axiosPostFilesData("/upload", { data: formData });
};

export const addReview = ({ id, data }) =>
  axiosPost("/reviews/product/" + id, { data });

export const getReview = (id) => axiosGet("/reviews/list/" + id.id);

export const updateReview = ({ id, ...data }) =>
  axiosPatch("/reviews/update/" + id, { data });

export const updateProduct = ({ id, ...data }) =>
  axiosPatch("/products/update/" + id, { data });

export const deleteReview = ({ id }) => axiosDelete("/reviews/delete/" + id);
