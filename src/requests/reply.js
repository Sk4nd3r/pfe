import { axiosPost, axiosGet, axiosPatch } from "./http";

export const addReply = ({ id, ...data }) =>
  axiosPost("/replies/review/" + id, { data });
export const getReply = (params) => axiosGet("/replies/", { params });
export const updateReply = (data) => axiosPatch("/cart/", { data });
