import { axiosPostFilesData, axiosPost, axiosGet, axiosPatch } from "./http";

export const addToFavorite = (data) => axiosPost("/favorite/"  ,{ data });
export const getFavorite = (params) => axiosGet("/favorite/"  ,{ params });
export const updateFavorite = (data) => axiosPatch("/favorite/update"  ,{ data });
