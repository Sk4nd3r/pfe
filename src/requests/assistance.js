import { axiosPost, axiosGet, axiosPatch } from "./http";

export const createContact = (data) =>
  axiosPost("/assistance/create", { data });
export const getContact = (params) =>
axiosGet("/assistance/", { params });
