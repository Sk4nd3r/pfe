import { axiosPost, axiosGet, axiosPatch } from "./http";

export const addReport = ({ id, ...data }) =>
  axiosPost("/reports/review/" + id, { data });
export const getReport = (params) => axiosGet("/reports/", { params });
