export function readAsDataURL(blob){
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('loadend', (e) => {
      resolve((e.target?.result ) || '');
    });
    reader.addEventListener('error', (e) => {
      reject(e);
    });
    reader.readAsDataURL(blob);
  });
}

export function readAsText(blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.addEventListener('loadend', (e) => {
      resolve((e.target?.result ) || '');
    });
    reader.addEventListener('error', (e) => {
      reject(e);
    });
    reader.readAsText(blob);
  });
}
