import React, {
 useState, ChangeEvent, useRef, useCallback,
} from 'react';

import { isNaN, isArray } from 'lodash';
import { isNumberEmpty, isStringEmpty } from '../utils/validation';



export function useInput(
  initialValue
) {
  const [value, valueChange] = useState(initialValue);
  const touched = useRef(false);

  function onChange(e) {
    e.preventDefault();
    if (!touched.current) touched.current = true;
    valueChange(e.target.type === 'checkbox' ? e.target.checked : e.target.value);
  }
  return [value, onChange, touched.current];
}

export function useForm(options) {
  const validationFns = options.validation || {};
  const required = options.required || [];

  required.forEach((key) => {
    if (!validationFns[key]) {
      if (isArray(options.initialValues[key])) {
        (validationFns )[key] = (value) => (value.length ? '' : 'Champ vide');
      } else {
        (validationFns )[key] = typeof options.initialValues[key] === 'string' ? isStringEmpty : isNumberEmpty;
      }
    }
  });

  const initialTouched = {};
  const initialErrors = {};
  const keys = Object.keys(options.initialValues) ;
  let initialValues= {};
  keys.forEach((key) => {
    initialTouched[key] = false;
    const value = options.initialValues[key];
    const errorFn = validationFns[key];
    if (errorFn) {
      initialErrors[key] = errorFn(value);
    }
    initialValues[key] = (isNaN(options.initialValues[key]) ? '' : options.initialValues[key]);
  });
  const [values, valuesChange] = useState(initialValues);
  const [touched, touchedChange] = useState(initialTouched);
  const [errors, errorsChange] = useState(initialErrors);

  function changeObject(obj,newObject) {
    return { ...obj, ...newObject };
  }

  function validateForm() {
    const valuesFind= Object.keys(values) ;
    return !valuesFind.find((key) => errors[key]);
  }

  function setTouched(nextTouched) {
    touchedChange(changeObject(touched, nextTouched));
  }

  function setAllTouched(value) {
    const nextTouched = {};
    const touchedKeys= Object.keys(values);
    touchedKeys.forEach((key) => {
      nextTouched[key] = value;
    });
    touchedChange(changeObject(touched, nextTouched));
  }

  function setErrors(nextErrors) {
    errorsChange(changeObject(errors, nextErrors));
  }

  function setValues(nextValues) {
    const valuesKeys = (Object.keys )(nextValues);
    const nextErrors= {};
    const v= {} ;
    valuesKeys.forEach((key) => {
      const validationFn = validationFns[key];
      if (validationFn) {
        nextErrors[key] = validationFn(nextValues[key] );
      }
      v[key] = (isNaN(nextValues[key]) ? '' : nextValues[key]) ;
    });

    setErrors(nextErrors);
    valuesChange(changeObject(values, v));
  }

  function handleChange(e) {
    if (e.target.type !== 'checkbox') {
      e.preventDefault();
    }

    let { value } = e.target ;
    if (e.target.type === 'checkbox') value = e.target.checked;
    else if (e.target.type === 'file' && e.target.files) [value] = e.target.files;

    const name = e.target.name;
    if (!touched[name]) {
      setTouched({ [name]: true } );
    }

    setValues({ [name]: value } );
  }

  return [
    { values, errors, touched },
    {
      handleChange: useCallback(handleChange, [values, touched, errors]),
      validateForm: useCallback(validateForm, [values, touched, errors]),
      setValues: useCallback(setValues, [values, touched, errors]),
      setErrors: useCallback(setErrors, [values, touched, errors]),
      setTouched: useCallback(setTouched, [values, touched, errors]),
      setAllTouched: useCallback(setAllTouched, [values, touched, errors]),
    },
  ];
}
