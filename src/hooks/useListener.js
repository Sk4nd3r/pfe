import { useEffect, useRef } from 'react';

export function useListener(
  type,
  listener,
  option
) {
  const mounted = useRef(false);

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true;
    } else {
      window.removeEventListener(type, listener, options);
    }
    window.addEventListener(type, listener);
    return () => window.removeEventListener(type, listener, options);
  });
}
