import { Ref, useEffect } from 'react';

export default function useCaptureRef(value, ref) {
  useEffect(() => {
    if (ref) {
      if (typeof ref === 'function') ref(value);
      // eslint-disable-next-line
      else (ref.current ) = value;
    }
    return () => {
      if (ref) {
        if (typeof ref === 'function') ref(null);
        // eslint-disable-next-line
        else (ref.current ) = null;
      }
    };
  });
}
