import { useState, MouseEvent, FocusEvent } from 'react';

export function useHover(
  initialValue,
){
  const [value, valueChange] = useState(initialValue);
  function onHoverIn(e) {
    if (e) {
      e.preventDefault();
    }
    valueChange(true);
  }
  function onHoverOut(e) {
    if (e) {
      e.preventDefault();
    }
    valueChange(false);
  }
  return [value, onHoverIn, onHoverOut];
}

export function useFocus(
  initialValue
) {
  const [value, valueChange] = useState(initialValue);
  function onFocus() {
    valueChange(true);
  }
  function onBlur() {
    valueChange(false);
  }
  return [value, onFocus, onBlur];
}
